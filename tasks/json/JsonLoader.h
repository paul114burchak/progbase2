typedef struct Auto Auto;
typedef struct Driver Driver;

void JsonLoader_saveToString(char * str, const Driver * driver);  // 1
void JsonLoader_loadFromString(Driver * driver, const char * jsonString);  // 2