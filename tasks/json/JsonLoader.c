#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "JsonLoader.h"
#include <jansson.h>

struct Auto {
	char model[30];
	int seats;
};

struct Driver {
	char name[30];
	int age;
	float tariff; 
	struct Auto car[4];
};

void JsonLoader_saveToString(char * str, const Driver * driver) {
    json_t * json = json_object();
    json_object_set_new(json, "name", json_string(driver->name));
    json_object_set_new(json, "age", json_integer(driver->age));
    json_object_set_new(json, "tariff", json_real(driver->tariff));

    json_t* arr = json_array();

    for(int i = 0; i < sizeof(driver->car) / sizeof(Auto); i++) {
        json_t * item = json_object();
        json_object_set_new(item, "model" , json_string((driver->car)[i].model));
        json_object_set_new(item, "seats" , json_integer((driver->car)[i].seats));
        json_array_insert_new(arr,i,item);
    }
    json_object_set_new(json, "autos", arr);
    // create JSON document string
    char * jsonString = json_dumps(json, JSON_INDENT(2));
    strcpy(str, jsonString);
    free(jsonString); 

    json_decref(json);
}

void JsonLoader_loadFromString(Driver * driver, const char * jsonStr) {
	json_error_t err;
	json_t * jsonArr = json_loads(jsonStr, 0, &err);
	strcpy(driver->name , (char *)json_string_value(json_object_get(jsonArr, "name")));
	driver->age = json_integer_value(json_object_get(jsonArr, "age"));
	driver->tariff = json_real_value(json_object_get(jsonArr, "tariff"));

    json_t * groupObj = json_object_get(jsonArr, "autos");
    
    for(int i = 0; i < sizeof(driver->car) / sizeof(Auto); i++) {
        json_t * autosArr = json_array_get(groupObj, i);
        strcpy(driver->car[i].model , (char *)json_string_value(json_object_get(autosArr, "model")));
        driver->car[i].seats = json_integer_value(json_object_get(autosArr, "seats"));
    }
    
	json_decref(jsonArr);

}