#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <thread>
#include <mutex>

#include <progbase.h>
#include <progbase-cpp/console.h>

#include <graphics.h>

using namespace std;
using namespace progbase::console;


mutex g_m;
void DrawHex (double side, CursorAttributes color) {
	Vec2D point1 = {
		.x = 105,
		.y = 12
	};
	double radius = side*sqrt(3)/2;
	Vec2D point2 = {
		.x = point1.x + side,
		.y = point1.y
	};
	Graphics_drawLine(NULL,point1,point2,color);
	point1.y += 2*radius;
	point2.y += 2*radius;
	Graphics_drawLine(NULL,point1,point2,color);
	point1.x += side + (2 * radius- side)/2;
	point1.y -= radius;
	Graphics_drawLine(NULL,point1,point2,color);
	point2.y -= 2*radius;
	Graphics_drawLine(NULL,point1,point2,color);
	point1.x -= 2*radius;
	point2.x -= side;
	Graphics_drawLine(NULL,point1,point2,color);
	point2.y += 2*radius;
	Graphics_drawLine(NULL,point1,point2,color);
}

void DrawRect(Vec2D p1, Vec2D p2, Vec2D p3, Vec2D p4,ConsoleColor color) {
	Graphics_drawPixel(NULL,p1,color);
	Graphics_drawLine(NULL,p2,p1,color);
	Graphics_drawLine(NULL,p4,p2,color);
	Graphics_drawLine(NULL,p3,p1,color);
	Graphics_drawLine(NULL,p4,p3,color);
}

void ClearSector(int start, int end) {
    Console::setCursorAttribute(CursorAttributes::BG_DEFAULT);
	for (double i = 0; i < 43; i++) {
		for (double j = start; j < end;j++) {
            Console::setCursorPosition(i,j);
            cout << " " << flush;
		}
	}
}

void RotateRectangular(double m, double n, int T, ConsoleColor color) {
    int degrees = 0;
    Vec2D d1 = {
    .x = 20,
    .y = 15
    };
    Vec2D d2 = {
    .x = d1.x+m,
    .y = d1.y
    };
    Vec2D d3 = {
    .x = d1.x,
    .y = d1.y+n
    };
    Vec2D d4 = {
    .x = d1.x+m,
    .y = d1.y+n
    };
    Vec2D centre = {
    .x = d1.x+m/2,
    .y = d1.y+n/2
    };
	while (1) {
        g_m.lock();
        ClearSector(1,74); //concole size on x axis
		Graphics_drawLine(NULL,{75,1},{75,43},pb::BG_CYAN);
        g_m.unlock();
        double radians = degrees * M_PI / 180.0;
        if (degrees == 360) degrees = 1;
        Vec2D p1 = Vec2D_rotate(d1,centre,radians);
        Vec2D p2 = Vec2D_rotate(d2,centre,radians);
    	Vec2D p3 = Vec2D_rotate(d3,centre,radians);
        Vec2D p4 = Vec2D_rotate(d4,centre,radians);

		g_m.lock();
        DrawRect(p1,p2,p3,p4,color);
        g_m.unlock();
		this_thread::sleep_for(chrono::milliseconds(T/450));
        degrees++;
	}
}

void PulseHexagon(double size,int A, int T, CursorAttributes color) {
	int d = 1;
	int dir = 1;
	while (1) {
		g_m.lock();
        ClearSector(76,155); //concole size on x axis
		Graphics_drawLine(NULL,{75,1},{75,43},pb::BG_CYAN);
		g_m.unlock();
		d+=dir;
		if (d == A) {
			dir = -1;
		}
		if (d == -A) {
			dir = 1;
		}

		g_m.lock();
		DrawHex(size+d,color);
		g_m.unlock();
		this_thread::sleep_for(chrono::milliseconds(T/13));
	}
}

int main(void) {
    double side = 10; // length of hexagon side
    int a = 3; // amplitude of hexagon pulsing a >= 2
    double s = 30; // length of rectangular
    double n = 10; // width of rect
    int tR = 4500; // period of rect rotation
    int tH = 1350; // period of hex pulsation
	CursorAttributes colorH = CursorAttributes::BG_BLUE;
	CursorAttributes colorR = CursorAttributes::BG_YELLOW;
	thread Rect(RotateRectangular, s, n, tR, colorR);
	thread Hex(PulseHexagon, side, a, tH,colorH);

	while (!Console::isKeyDown()) {sleepMillis(1000);}

    Rect.detach();
    Hex.detach();
    Console::reset();
	return 0;
}
