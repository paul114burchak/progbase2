#include <stdio.h>
#include <stdlib.h>
#include <progbase/net.h>

#define BUFFER_LEN 1024

int main(void) {
    //
    // create UDP client
    UdpClient * client = UdpClient_init(&(UdpClient){});
    IpAddress * serverAddress = IpAddress_init(&(IpAddress){}, "127.0.0.1", 9998);
    NetMessage * message = NetMessage_init(
        &(NetMessage){},  // value on stack
        (char[BUFFER_LEN]){},  // array on stack 
        BUFFER_LEN);
    puts("Commands: \nlist - get list of existed files in dir\nnew [filename] - creates new file\ndel [filename] - deletes a file ");
        
    while (1) {
        puts("Please, input request:");
        // @todo Read user input and create request message
        
        char request[100] = "";
        fgets(request, 100, stdin);
        NetMessage_setDataString(message, request);
        //
        //
        // blocking call to receive response data from server
        printf("Send string `%s` to server %s:%d\n",
			NetMessage_data(message), 
			IpAddress_address(serverAddress),
			IpAddress_port(serverAddress));
        // send request to server
        if(!UdpClient_sendTo(client, message, serverAddress)) {
            perror("send");
            return 1;
        }    
        // @todo Show server response to user
        if(!UdpClient_receiveFrom(client, message, serverAddress)) {
            perror("recv");
            return 1; 
        }
        printf("Response received from server (%d bytes): %s\r\n", 
            NetMessage_dataLength(message),
            NetMessage_data(message));
    }
    //
    // close client
    UdpClient_close(client);
    return 0;
}
