#include <stdio.h>
#include <stdlib.h>
#include <progbase/net.h>

#include <errno.h>
#include <string.h>
#include <ctype.h>
#define BUFFER_LEN 1024

typedef struct {
	char command[100];
	char name[100];
} Request;

Request parseRequest(const char * msgStr) {
	Request req = {
		.command = "",
		.name = ""
	};

	int n = 0;
	while(isalpha(msgStr[n])) n++;

	strncpy(req.command, msgStr, n);
	req.command[n] = '\0';

    n++;
    int i = 0;
	while(isalnum(msgStr[n]) || ispunct(msgStr[n])) {
        req.name[i] = msgStr[n];
        n++;
        i++;
    }    
    req.name[i] = '\0';
	return req;
}

void printRequest(Request * req) {
	printf("Request: `%s` `%s`\n",
		req->command,
		req->name);
}

char * ReadText(const char * readFileName) {
	FILE * fin = fopen(readFileName, "r");
	unsigned int N = 10, delta=10, i = 0;   
	char* buf = (char*) malloc (sizeof(char)*N);    
	while ((buf [i] = fgetc(fin)) != EOF  )  {                
		if (++i >= N) {
			N += delta;
			buf = (char*) realloc (buf, sizeof(char)*N);        
		}
	}
	buf[i] = '\0';
	fclose(fin);
	return buf;
}

int main(void) {
    //
    // create UDP server
    UdpClient * server = UdpClient_init(&(UdpClient){});
    IpAddress * address = IpAddress_initAny(&(IpAddress){}, 9998);
    if (!UdpClient_bind(server, address)) {
        perror("Can't start server");
        return 1;
    }
    printf("Udp server started on port %d\n", 
        IpAddress_port(UdpClient_address(server)));
    
    NetMessage * message = NetMessage_init(
        &(NetMessage){},  // value on stack
        (char[BUFFER_LEN]){},  // array on stack 
        BUFFER_LEN);

    IpAddress clientAddress;
    while (1) {
        puts("Waiting for data...");
        //
        // blocking call to receive data from clients
        if(!UdpClient_receiveFrom(server, message, &clientAddress)) {
			perror("recv");
			return 1;
		}
        printf("Received message from %s:%d (%d bytes): `%s`\n",
            IpAddress_address(&clientAddress),  // client IP-address
            IpAddress_port(&clientAddress),  // client port
            NetMessage_dataLength(message),
            NetMessage_data(message));
            
        // @todo Process clients input and send response
        const char * msgStr = NetMessage_data(message);
		Request req = parseRequest(msgStr);
		printRequest(&req);
        if(0 == strcmp("list",req.command)) {
            system("touch temp");
            system("find . -maxdepth 1 -type f > temp");
            char *buf = ReadText("temp");
            system("rm temp");
            NetMessage_setDataString(message, buf);
        } else if(0 == strcmp("new", req.command)) {
            char out[106] = "touch ";
            strcat(out,req.name);
            system(out);

            char mess[30] = " New file [";
            strcat(mess, req.name);
            strcat(mess, "] was created");
            NetMessage_setDataString(message, mess);
            } else if(0 == strcmp("del", req.command)) {
                char out[103] = "rm ";
                strcat(out,req.name);
                system(out);

                char mess[30] = " Now the file [";
                strcat(mess, req.name);
                strcat(mess, "] doesn't exists");
                NetMessage_setDataString(message, mess);
                } else {
                    NetMessage_setDataString(message, "Error: unrecognized request");
                }
        
        // send echo response
        if (!UdpClient_sendTo(server, message, &clientAddress)) {
			perror("send");
			return 1;
		}
    }
    //
    // close server
    UdpClient_close(server);
    return 0;
}
