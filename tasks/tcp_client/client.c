#include <stdio.h>
#include <stdlib.h>
#include <progbase/net.h>
#include <string.h>
#include <jansson.h>
#include <progbase/list.h>

#define BUFFER_LEN 1024

int main(void) {
    TcpClient * client = TcpClient_init(&(TcpClient){});
    IpAddress * serverAddress = IpAddress_init(&(IpAddress){}, "127.0.0.1", 9995);
    if (!TcpClient_connect(client,serverAddress)) {
        perror("connect");
        return 1;
    }
    NetMessage * message = NetMessage_init(
        &(NetMessage){},  // value on stack
        (char[BUFFER_LEN]){},  // array on stack 
        BUFFER_LEN);

    puts("Please, input request:");
    
    char request[100] = "";
    fgets(request, 100, stdin);
    NetMessage_setDataString(message, request);

    printf("Send string `%s` to server %s:%d\n",
        NetMessage_data(message), 
        IpAddress_address(serverAddress),
        IpAddress_port(serverAddress));
    // send request to server
    if(!TcpClient_send(client, message)) {
        perror("send");
        return 1;
    }    
    // @todo Show server response to user
    if(!TcpClient_receive(client, message)) {
        perror("recv");
        return 1; 
    }
     char str[BUFFER_LEN];
     strcpy(str,NetMessage_data(message));
    // close client
     TcpClient_close(client);
    // printf("%s",str);
    json_error_t err;
    json_t * resp = json_loads(str,0,&err);
    json_t * arr = json_object_get(resp,"data");
    json_t * status = json_object_get(resp,"status");
    List * vector = List_new();
    int mass[json_array_size(arr)];
	json_t * value = NULL;
    int index = 0;
    json_array_foreach(arr,index,value) {
        mass[index] = (int)json_integer_value(value);
        List_add(vector,(void *)&mass[index]);
    }
    if (json_boolean_value(status) == false) {
        printf("Response received from server (%d bytes): %s\r\n", 
            NetMessage_dataLength(message),
            NetMessage_data(message));
    } else {
        printf("\nVector received from server:");
        for (int j = 0; j < index; j++) printf("| %i |",*(int*)List_get(vector,j));
        List * newVector = List_new();
        List_add(newVector,List_get(vector,1));
        for (int j = 1; j < index-1; j++) {
            int *el1 = (int *)List_get(vector,j-1);
            int *el2 = (int *)List_get(vector,j+1);
            int * temp = malloc(sizeof(int));
            *temp = *el1 + *el2;
            List_add(newVector,(void*)temp);
        }
        List_add(newVector,List_get(vector,index-2));
        printf("\nNewly computed vector");
        for (int j = 0; j < index; j++) printf("| %i |",*(int*)List_get(newVector,j));
        List_free(&newVector);
        printf("Сформувати новий вектор цілих числел, що містить попарні суми сусідніх елементів \
вхідного вектора.");
    }
	json_decref(arr);
    List_free(&vector);
    return 0;
}