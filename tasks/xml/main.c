#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "XmlLoader.h"

struct Auto {
	char *model;
	int seats;
};

struct Driver {
	char *name;
	int age;
	float tariff; 
	struct Auto car[4];
};

int main(void) {
    Driver driver = {
		.name = "Hans",
		.age = 27,
		.tariff = 14.18,
		.car[0] = {
			.model = "VolksWagen",
			.seats = 5,
		},
		.car[1] = {
			.model = "Daimler-Benz",
			.seats = 4,
		},
		.car[2] = {
			.model = "BMW",
			.seats = 6,
		},
		.car[3] = {
			.model = "Porsche",
			.seats = 2,
		}
	};

	char str[1000];
    XmlLoader_saveToString(str,&driver);
    
	printf("XmlString, after copying from struct\n");
    for (int i = 0; str[i] != '\0'; i++) {
		printf("%c", str[i]);
	}
	// new empty struct
    Driver newDriver; 
    XmlLoader_loadFromString(&newDriver,str);

	printf("\nStruct after copying from string\n");
	printf("Name: %s\n", newDriver.name);
	printf("Age: %i\n", newDriver.age);
	printf("Tariff: %.3f\n", newDriver.tariff);
	for (int i = 0; i < sizeof(newDriver.car) / sizeof(Auto); i++){
		printf("Auto №%i: Model - %s;\n 	NumOfSeats - %i\n", i + 1, newDriver.car[i].model, newDriver.car[i].seats);
	}
return 0;
}
