#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "XmlLoader.h"
#include <libxml/parser.h>
#include <libxml/tree.h>

struct Auto {
	char *model;
	int seats;
};

struct Driver {
	char *name;
	int age;
	float tariff; 
	struct Auto car[4];
};

void XmlLoader_saveToString(char * str, Driver * driver) {
	xmlDoc * doc = NULL;
	xmlNode * rootNode = NULL;
	xmlNode * driverNode = NULL;
	xmlNode * autoNode = NULL;
	xmlNode * autosNode = NULL;
	char strBuf[1000];
	
	doc = xmlNewDoc(BAD_CAST "1.0");

	// create xml tree

	// create one root element
	rootNode = xmlNewNode(NULL, BAD_CAST "drivers");
	xmlDocSetRootElement(doc, rootNode);
	
	driverNode = xmlNewChild(rootNode, NULL, BAD_CAST "driver", NULL);
	xmlNewProp(driverNode, BAD_CAST "name", BAD_CAST driver->name);
	sprintf(strBuf, "%i", driver->age);  // copy number to string
	xmlNewProp(driverNode, BAD_CAST "age", BAD_CAST strBuf);
	sprintf(strBuf, "%f", driver->tariff);  // copy number to string
	xmlNewProp(driverNode, BAD_CAST "tariff", BAD_CAST strBuf);
	// create Auto element as student child
	autosNode = xmlNewChild(driverNode, NULL, BAD_CAST "autos", NULL);

	for (int i = 0; i < sizeof(driver->car) / sizeof(Auto);i++){
		autoNode = xmlNewChild(autosNode, NULL, BAD_CAST "auto", NULL);
		xmlNewChild(autoNode, NULL, BAD_CAST "model", BAD_CAST driver->car[i].model);
		sprintf(strBuf, "%i", driver->car[i].seats);
		xmlNewChild(autoNode, NULL, BAD_CAST "seats", BAD_CAST strBuf);
	}

	// copy xml contents to char buffer
	xmlBuffer * bufferPtr = xmlBufferCreate();
	xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
	for (int i = 0; bufferPtr->content[i] != '\0'; i++) {
		str[i] = bufferPtr->content[i];
	}
	xmlBufferFree(bufferPtr);
	
    xmlFreeDoc(doc);
}

void XmlLoader_loadFromString(Driver * driver, const char * xmlStr) {
    xmlDoc * xDoc = xmlReadMemory(xmlStr, strlen(xmlStr), NULL, NULL, 0);
    if (NULL == xDoc) {
        printf("Error parsing xml");
    }

    xmlNode * xRootEl = xmlDocGetRootElement(xDoc);
	xmlNode * xDriver = xmlFirstElementChild(xRootEl);
	xmlNode * xAutos = xmlFirstElementChild(xDriver);

	driver->name = (char*)xmlGetProp(xDriver,  BAD_CAST "name");
	char* ageStr = (char*)xmlGetProp(xDriver,  BAD_CAST "age"); 
	int age = atoi(ageStr);
	driver->age = age; 
	char* tariffStr= (char*)xmlGetProp(xDriver,  BAD_CAST "tariff"); 
	float tariff = atof(tariffStr);
	driver->tariff = tariff;
	free(ageStr);
	free(tariffStr);

        if (XML_ELEMENT_NODE == xDriver->type) {
			int i = -1;
			for (xmlNode * xAuto = xAutos->children; NULL != xAuto ; xAuto = xAuto->next) {
				if (XML_ELEMENT_NODE == xAuto->type) {
					if (xmlStrcmp(xAuto->name, BAD_CAST "auto") == 0){
						i++;
						for (xmlNode * xJ = xAuto->children; NULL != xJ ; xJ = xJ->next) {
							if (XML_ELEMENT_NODE == xJ->type) {
								if (xmlStrcmp(xJ->name, BAD_CAST "model") == 0){
									driver->car[i].model = (char *) xmlNodeGetContent(xJ);
								} 
								if(xmlStrcmp(xJ->name, BAD_CAST "seats") == 0){
									char* seatsStr = (char *) xmlNodeGetContent(xJ);
									int seats = atoi(seatsStr);
									driver->car[i].seats = seats;
									free(seatsStr);
								}
							}
						}
					}
				}
			}   
   		 }
	xmlFreeDoc(xDoc);
}
