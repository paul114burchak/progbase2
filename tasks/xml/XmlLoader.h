typedef struct Auto Auto;
typedef struct Driver Driver;

void XmlLoader_saveToString(char * str, Driver * driver);  // 1
void XmlLoader_loadFromString(Driver * driver, const char * xmlStr);  // 2
//Функція 1 через вказівник на сутність зберігає її вміст у строку у форматі XML (при цьому потрібно використати хоча б один XML-атрибут).
//Функція 2 зі строки із XML вмістом заповнює поля сутності через вказівник на неї.
