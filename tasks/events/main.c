#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <list.h>
#include <events.h>
#include <pbconsole.h>
#include <progbase.h>

/* custom constant event type ids*/
enum {
	KeyInputEventTypeId,
	RandomNumberEventTypeId,
	EnteringInRoomTypeId,
	EnteringOutRoomTypeId,
	SWITCHON, SWITCHOFF
};

/* event handler functions prototypes */
void RandomNumberGenerator_update(EventHandler * self, Event * event);
void UpdatePrintHandler_update(EventHandler * self, Event * event);
void SwitchLight_handleEvent(EventHandler * self, Event * event);
void KeyInputHandler_update(EventHandler * self, Event * event);
void ListOut_handleEvent(EventHandler * self, Event * event);
void ListIn_handleEvent(EventHandler * self, Event * event);

int main(void) {
	// startup event system
	EventSystem_init();

	// add event handlers
	EventSystem_addHandler(EventHandler_new(NULL, NULL, UpdatePrintHandler_update));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, RandomNumberGenerator_update));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, KeyInputHandler_update));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, SwitchLight_handleEvent));
	List * outRoom = List_new();
	List * inRoom = List_new();
	List_add(outRoom,"Taras");
	List_add(outRoom,"Petro");
	List_add(outRoom,"Ivan");
	List_add(outRoom,"Vasya");
	List_add(outRoom,"Ruslan");
	List_add(outRoom,"Anna");
	List_add(outRoom,"Maria");
	List_add(outRoom,"Paul");

	EventSystem_addHandler(EventHandler_new(outRoom, free, ListOut_handleEvent));
	EventSystem_addHandler(EventHandler_new(inRoom, free, ListIn_handleEvent));

	// start infinite event loop
	EventSystem_loop();
	// cleanup event system
	EventSystem_cleanup();
	List_free(&outRoom);
	List_free(&inRoom);
	return 0;
}

/* event handlers functions implementations */

void UpdatePrintHandler_update(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			puts("");
			puts("<<<START!>>>");
			puts("Press [Esc] to exit");
			puts("");
			break;
		}
		case UpdateEventTypeId: {
			putchar('.');
			fflush(stdout);
			sleepMillis(75);
			break;
		}
		case ExitEventTypeId: {
			puts("");
			puts("<<<EXIT!>>>");
			puts("");
			break;
		}
		case KeyInputEventTypeId: {
			char * keyCodePtr = (char *)event->data;
			if (*keyCodePtr == 27) {
				EventSystem_exit();	
			}
			break;
		}
	}
}

void KeyInputHandler_update(EventHandler * self, Event * event) {
	if (conIsKeyDown()) {  // non-blocking key input check
		char * keyCode = malloc(sizeof(char));
		*keyCode = getchar();
		EventSystem_raiseEvent(Event_new(self, KeyInputEventTypeId, keyCode, free));
	}
}

void RandomNumberGenerator_update(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			srand(time(0));
			break;
		}
		case UpdateEventTypeId: {
			int * number = malloc(sizeof(int));
			*number = rand() % 100;
			EventSystem_raiseEvent(Event_new(self, RandomNumberEventTypeId, number, free));
			break;
		}
	}
}

void ListOut_handleEvent(EventHandler * self, Event * event) {
	switch (event->type) {
		case RandomNumberEventTypeId: {
			List * list = (List *)self->data;
			int * number = (int *)event->data;
			if (List_count(list) == 0) {
				printf("Everybody is in room\n");
				EventSystem_exit();
			}
			if (*number < List_count(list) && *number > -1) {
				char * name = List_get(list,*number);
				printf("%i was genereted........", *number);
				printf("%s entered in room\n",name);
				List_removeAt(list,*number);
				EventSystem_raiseEvent(Event_new(self, EnteringInRoomTypeId, name, NULL));
			}
			break;
		}
		case EnteringOutRoomTypeId: {
			char * name = (char *)event->data;
			List_add(self->data,name);
			break;
		}
	}
}

void ListIn_handleEvent(EventHandler * self, Event * event) {
	switch (event->type) {
		case EnteringInRoomTypeId: {
			char * name = (char *)event->data;
			List_add(self->data,name);
			int *count = malloc(sizeof(int));
			*count = List_count(self->data);
			EventSystem_raiseEvent(Event_new(NULL,SWITCHON,count,NULL));
			break;
		}
		case RandomNumberEventTypeId: {
			List * list = (List *)self->data;
			int * number = (int *)event->data;
			int toRnd = 10;
			if (*number-toRnd < List_count(list) && *number-toRnd > 0) {
				char * name = List_get(list,*number-toRnd);
				printf("%i was genereted........", *number-toRnd);
				printf("%s went out of room\n",name);
				List_removeAt(list,*number-toRnd);
				if (List_count(list) == 0) EventSystem_raiseEvent(Event_new(NULL,SWITCHOFF,NULL,NULL));
				EventSystem_raiseEvent(Event_new(self, EnteringOutRoomTypeId, name, NULL));
			}
			break;
		}
	}
}

void SwitchLight_handleEvent(EventHandler * self, Event * event) {
	switch (event->type) {
		case SWITCHON: {
			int * number = (int *)event->data;
			printf("LIGHT SWITCHED ON. %i peoples in room",*number);
			break;
		}
		case SWITCHOFF: {
			printf("LIGHT SWITCHED OFF\n");
			break;
		}
		case StartEventTypeId: {
			printf("LIGHT SWITCHED OFF\n");
			break;
		}
	}
}