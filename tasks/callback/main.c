#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

enum {SIZE = 10};
void task1(void);
int cmpTask1(const void * a, const void * b);
void task2(void);
int cmpTask2(const void * a, const void * b);
void task3(void);
void action(int a, int i);
void Array_foreach(int arr[], int len, void (*act)(int, int));
void Array_foreachReversed(int arr[], int len, void (*act)(int, int));

typedef struct {
    char surname[30];
    int score;
} Student;

int main(void) {
    task1();
    task2();
    task3();
return 0;
}

void task1(void) {
    char words[SIZE][20] = {
        {"Alpha"},
        {"Beta"},
        {"Gamma"},
        {"Delta"},
        {"Epsilon"},
        {"Omega"},
        {"Theta"},
        {"BigOh"},
        {"Eye"},
        {"football"}
    };

    puts("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<TASK1>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    for (int i = 0; i < SIZE;i++) printf("%s, ",words[i]);
    printf("\nВикористовуючи стандартну функцію qsort() і callback-функції, виконати сортування масиву слів\nу порядку зростання кількості приголосних букв.\n");
    qsort(words,SIZE,sizeof(char)*20,cmpTask1);
    for (int i = 0; i < SIZE;i++) printf("%s, ",words[i]);
}

int cmpTask1(const void * a, const void * b) {
    const char consonants[] = "qwrtpsdfghjklzxcvbnm";
    const char (*left) = (const char *)a;
    const char (*right) = (const char *)b;
    
    char * pch = strpbrk (left, consonants);
    int LCount = 0, RCount = 0;
    while (pch != NULL) {
        LCount++;
        pch = strpbrk (pch+1,consonants);                                
    }

    pch = strpbrk (right, consonants);
    while (pch != NULL) {
        RCount++;
        pch = strpbrk (pch+1,consonants);                                
    }
    return LCount - RCount;
}

void task2(void) {
    Student students[SIZE] = {
        {"andrievskiy",80},
        {"sadritskiy",14},
        {"burchak",88},
        {"zgurovskiy",79},
        {"lagoduk",9000},
        {"kololviets",78},
        {"iliyn",28},
        {"telepuz",99},
        {"masluk",100},
        {"dovgopol",85},
    };
    
    puts("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<TASK2>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    for (int i = 0; i < 10;i++) printf("%s, %i\n",students[i].surname,students[i].score);
    qsort(students,10,sizeof(Student),cmpTask2);
    printf("Описати структуру Студент і за допомогою qsort(), виконати сортування масиву \nелементів Студент за прізвищем у алфавітному порядку.\n");
    for (int i = 0; i < 10;i++) printf("%s, %i\n",students[i].surname,students[i].score);
}


int cmpTask2(const void * a, const void * b) {
	const Student s1 = *(const Student *)a; 
	const Student s2 = *(const Student *)b; 
    int flag = 0;
		int min = strlen(s1.surname);
		if (min > strlen(s2.surname)) min = strlen(s2.surname);
		for (int i = 0; i < min; i++) {
			int sur1 = s1.surname[i];
			int sur2 = s2.surname[i];
			if(sur1 == sur2) continue;
            else if(sur1>sur2) {
				flag = 1;
				break;
			} else {
				flag = -1;
				break;
			}
		}
    if (flag == 0) {
        if (strlen(s1.surname) > strlen(s2.surname)) return 1;
        else if (strlen(s1.surname) < strlen(s2.surname)) return -1;
    }
	return flag;
}

void task3(void) {
    void (*PtrAction) (int, int) = action;
    int arr[SIZE] = {-1,0,4,-24,-5,9,-13,3,-8,-5};
    puts("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<TASK3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    puts("Array");
    for (int i = 0; i < SIZE; i++) printf("%i ", arr[i]);
    Array_foreach(arr,SIZE,PtrAction);
    Array_foreachReversed(arr,SIZE,PtrAction);
}

void action(int a, int i) {
    if (a < 0 && abs(a) % 2 != 0) printf("Index:%i Key: %i\n",i,a);
}

void Array_foreach(int arr[], int len, void (*act)(int, int)) {
    printf("\nstraight order\n");
    for (int i = 0; i < len; i++) act(arr[i],i);
}

void Array_foreachReversed(int arr[], int len, void (*act)(int, int)) {
    printf("\nreverse order\n");
    for (int i = len-1; i >= 0; i--) act(arr[i],i);
}