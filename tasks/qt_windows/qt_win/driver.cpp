#include "driver.h"

Driver::Driver()
{
    this->_name = "";
    this->_age = 0;
    this->_tariff = 0.0;
}

void Driver::set_name(string name) { this->_name = name; }
void Driver::set_age(int age) { this->_age = age; }
void Driver::set_tariff(float tariff) { this->_tariff = tariff; }

string Driver::name() { return this->_name; }
int Driver::age() { return this->_age; }
float Driver::tariff() { return this->_tariff; }
