#-------------------------------------------------
#
# Project created by QtCreator 2017-03-31T21:12:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_win
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    driver.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    driver.h

FORMS    += mainwindow.ui \
    dialog.ui
