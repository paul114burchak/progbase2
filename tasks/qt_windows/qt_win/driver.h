#ifndef DRIVER_H
#define DRIVER_H

#include <iostream>

using namespace std;

class Driver
{
    string _name;
    int _age;
    float _tariff;
public:
    Driver();

    void set_name(string name);
    void set_age(int age);
    void set_tariff(float tariff);

    string name();
    int age();
    float tariff();
};

#endif // DRIVER_H
