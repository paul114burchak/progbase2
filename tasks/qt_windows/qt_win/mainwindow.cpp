#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "driver.h"
#include <QCloseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QLineEdit * nameEdit = this->findChild<QLineEdit*>("lineEdit");
    QLineEdit * ageEdit = this->findChild<QLineEdit*>("lineEdit_2");
    QLineEdit * tariffEdit = this->findChild<QLineEdit*>("lineEdit_3");

    QString name = nameEdit->text();
    QString ageStr = ageEdit->text();
    QString tariffStr = tariffEdit->text();

    bool isInt;
    int age = ageStr.toInt(&isInt);
    bool isFloat;
    float tariff = tariffStr.toFloat(&isFloat);

    Driver driver = Driver();

    driver.set_name(name.toStdString());
    if (isInt) driver.set_age(age);

    if(isFloat) driver.set_tariff(tariff);

    Dialog *dialog = new Dialog();

    dialog->show();
    dialog->setWindowTitle("Info");
    if (isInt && isFloat) {
        QLabel * label5 = dialog->findChild<QLabel*>("label_5");
        QLabel * label6 = dialog->findChild<QLabel*>("label_6");
        QLabel * label7 = dialog->findChild<QLabel*>("label_7");

        QString nameToOut = QString::fromStdString(driver.name());
        QString ageStrToOut = QString::number(driver.age());
        QString tariffStrToOut = QString::number(driver.tariff());

        label5->setText(nameToOut);
        label6->setText(ageStrToOut);
        label7->setText(tariffStrToOut);
    } else {
        QLabel * label = dialog->findChild<QLabel*>("label");
        QString err = QString::fromStdString("Invalid data");
        label->setText(err);
    }
}

