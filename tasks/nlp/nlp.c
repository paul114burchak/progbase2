#include "nlp.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

struct Word {
	char str[100];
	Word *next;
};

struct Sentence {
	Sentence *next;
	Word *head;
};

struct Text {
	Sentence *head;
};

Word * Word_new (char * str) {
	Word * self = malloc(sizeof(Word));
	self->next = NULL;
	//self->str = str;
	strcpy(self->str,str);
	return self;
}

void Word_free(Word **self) {
	assert(NULL != self);
	free(*self);
	*self = NULL;
}

Sentence * Word_addLast(Sentence * self, char * str) {
	Word * node = Word_new(str);
	Word * cur = self->head;
	if (cur == NULL) {
		cur = node;
	} else {
	while (cur->next != NULL) {
		cur = cur->next;
	}
	cur->next = node;
	}
	return self;
}


Sentence* addWords(Sentence * self, char * str) {
	for (int i = 0; i < sizeof(str);i++) {
		if(str[i+1] == ' ') {
			char* word;
			word = malloc(sizeof(char)*i);
			int j = 0;
			for(; j < i;j++) {
				word[j] = str[j];
			}
			self = Word_addLast(self,word);
			free(word);
		}
	}
	return self;
}

Sentence * Sentence_new(char * str) {
	Sentence * self = (Sentence *)malloc(sizeof(Sentence));
	self->next = NULL;
	self->head = NULL;
	self = addWords(self, str);
	return self;
}

void Sentence_free(Sentence ** self) {
	assert(NULL != self);
	free(*self);
	*self = NULL;
}

Text* Sentence_addLast(Text * self, char * str) {
	Sentence * node = Sentence_new(str);
	if (self->head == NULL) {
		node->next = self->head;
		self->head = node;
	}
	Sentence * cur = self->head;
	while (cur->next != NULL) {
    	  cur = cur->next;
	}
	cur->next = node;
	return self;
}

int Word_search(Sentence * self, char * str) {
	Word * node = self->head;
	int i = -1;
	while(node != NULL || strcmp(node->str,str) == 1) {
		node = node->next;
		i++;
	}
	return i;
}

void Word_removeAt(Sentence * self, int position) {
	assert(position > 0);
	if (position == 0) {
		Word_removeFirst(self);
	} else {
		int i = 0;
		Word * cur = self->head;
		while (cur->next->next != NULL && i != position - 1) {
			i += 1;
			cur = cur->next;
		}
		if (i != position - 1) assert(0);
		Word * node = cur->next;
		cur->next = node->next;
		Word_free(&node);
	}
}

void Word_removeFirst(Sentence * self) {
	Word * node = self->head;
	if (node == NULL) assert(0);
	self->head = node->next;
	Word_free(&node);
}

Text * Text_new() {
	struct Text * node = (struct Text *)malloc(sizeof(struct Text));
	node->head = NULL;
	return node;
}

void Text_free(Text ** self) {
	assert(NULL != self);
	free(*self);
	*self = NULL;
}

int Word_count(Sentence * self) {
	int count = 0;
	Word * cur = self->head;
	while (cur != NULL) {
    	  count += 1;
    	  cur = cur->next;
	}
	return count;
}
