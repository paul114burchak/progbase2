#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "file.h"
#include "nlp.h"

struct Word {
	char *str;
	Word *next;
};

struct Sentence {
	Sentence *next;
	Word *head;
};

struct Text {
	Sentence *head;
};

int main(void) {
	unsigned int i = 0;
	Text* head = Text_new();
	char* text = ReadText("input1.txt",i);
	unsigned int s = 0;
	Sentence_newFromString(text,s,head,i);
	free(text);
	int fp = File_writeText(head,"out.txt");
	if (fp == 1) puts("Error"); 
	else puts("Success");
	for (int i = 0; i < s; i++) {
		for(int j = 0; j < Word_count(head->head);j++) {
			Word_free(&head->head->head);
			head->head->head = head->head->head->next;
		}
		Sentence_free(&head->head);
		head->head = head->head->next;
	}
	Text_free(&head);
return 0;
}
