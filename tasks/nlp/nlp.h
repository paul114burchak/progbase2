#ifndef NLP_H
#define NLP_H

typedef struct Text Text;
typedef struct Sentence Sentence;
typedef struct Word Word;
Word * Word_new (char * str);
void Word_free(Word **self);
int Word_count(Sentence * self);
Sentence * Word_addLast(Sentence * self, char * str);
Sentence * Sentence_new(char * str);
void Sentence_free(Sentence ** self);
Text* Sentence_addLast(Text * self, char * str);
int Word_search(Sentence * self, char * str);
void Word_removeAt(Sentence * self, int position);
void Word_removeFirst(Sentence * self);
Sentence * addWords(Sentence * self, char * str);
Text * Text_new();
void Text_free(Text ** self);
#endif
