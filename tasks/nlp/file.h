#ifndef FILE_H
#define FILE_H
typedef struct _IO_FILE FILE;
#include "nlp.h"
char * ReadText(const char * readFileName, unsigned int i);
void File_writeWords(Word * word, FILE * fout);
void File_writeSentence(Sentence * sentence, FILE * fout);
int File_writeText(Text *text, const char * writeFileName);
void Sentence_newFromString(char * text, int s, Text * head, unsigned int l);
#endif