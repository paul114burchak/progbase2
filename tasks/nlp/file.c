#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "file.h"
#include "nlp.h"

struct Word {
	char *str;
	Word *next;
};

struct Sentence {
	Sentence *next;
	Word *head;
};

struct Text {
	Sentence *head;
};

char * ReadText(const char * readFileName, unsigned int i) {
	FILE * fin = fopen(readFileName, "r");
	unsigned int N = 10, delta=10;   
	char* buf = (char*) malloc (sizeof(char)*N);    
	while ((buf [i] = fgetc(fin)) != EOF  )  {                
		if (++i >= N) {
			N += delta;
			buf = (char*) realloc (buf, sizeof(char)*N);        
		}
	}
	buf[i] = '\0';
	fclose(fin);
	return buf;
}

void Sentence_newFromString(char* text, int s, Text * head, unsigned int l) {
	char * str;
	printf("%iu",l);
	for (int i=0; i < 57;i++) {
		if (text[i+1] == '.') {
			s++; // number of sentences
			str = malloc(sizeof(char)*i);
			for(int j = 0; j < i;j++) {
				str[j] = text[j];
				//printf("%c",text[j]);
			}
			head = Sentence_addLast(head,str);
			free(str);
		}
	}
}

/*char *text;
	text = (char *)malloc(10240);
	if (fin == NULL) {
		return NULL;
	}
	char buffer[500];
	while (fgets(buffer, 500, fin) != NULL) {
		buffer[strlen(buffer)] = '\0';
		strcat(text,buffer);
		for (int i = 0;i < strlen(buffer);i++) {
			if (buffer[i] == '.' || buffer[i] == '!' || buffer[i] == '?') {
				s++;
			}
		}
	}
	fclose(fin);*/

/*char* Sentence_newFromString(char * text, int s) {
	char delimiters[] = ".!?";
	char * parser = strtok(text,delimiters);
	char *sentence[s];
	int i = 0;
	while (parser != NULL) {
		strcpy(sentence[i++],parser);
		parser = strtok(NULL, delimiters);
	}
	return *sentence;	
}*/

void File_writeWords(Word * word, FILE * fout) {
	while(word != NULL) {
		printf("%s, ",word->str);
		word = word->next;
	}
}

void File_writeSentence(Sentence * sentence,FILE * fout) {
	while (sentence != NULL) {
		File_writeWords(sentence->head,fout);
		sentence=sentence->next;
	}
	
}

int File_writeText(Text *text, const char * writeFileName) {
	FILE * fout = fopen(writeFileName, "w");
	if (fout == NULL) return EXIT_FAILURE;
	File_writeSentence(text->head,fout);
	fclose(fout);
	return EXIT_SUCCESS;
}

