#include "circle.h"
#include <iostream>
#include <cmath>

using namespace std;
typedef string String;

Circle::Circle(float radius, String name, Point2d centre) {
    this->radius = radius;
    this->name = name;
    this->centre = centre;
}

float Circle::get_Square(){
    return M_PI*pow(this->radius,2);
}

void Circle::Circle_Print() {
    cout << "Radius: " << this->radius << endl;
    cout << "Coordinates of centre: (" << this->centre.x << ";" << this->centre.y << ")" << endl;
    cout << "Name: " << this->name << endl;
}
