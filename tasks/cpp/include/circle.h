#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>

using namespace std;
typedef string String;

struct Point2d {
    float x;
    float y;
};

class Circle{
    private:
        float radius;
        String name;
        Point2d centre;
    public:
        Circle(float radius, String name, Point2d centre);
        float get_Square();
        void Circle_Print();
};


#endif