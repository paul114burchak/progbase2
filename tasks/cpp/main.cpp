#include <iostream>
#include <vector>
#include "circle.h"

using namespace std;
void print_Data(vector<Circle*> &list, int i);

int main(void) {
    vector<Circle*> list;
    int i = 1;
    while (i > 0 && i < 4) {
        cout << "_____________________________________";
        cout << endl << "1 - print list content" << endl << "2 - add new element" << endl;
        cout << "3 - print all circles, square of that less then S" << endl << "    Push any button + Enter to exit" << endl;
        cout << "_____________________________________" << endl;
        cin >> i;
        switch (i) {
            case 1: { // prints all circles
                for(int i = 0; i != list.size(); i++) 
                    print_Data(list,i);
                break;
            }
            case 2: { // add new circle
                float rad;
                String name;
                Point2d coords;
                
                cout << "Enter radius:" << endl;
                cin >> rad;
                cout << "Enter name:" << endl;
                cin >> name;
                cout << "Enter coordinates of centre" << endl;
                cin >> coords.x >> coords.y;

                Circle * circle = new Circle(rad,name,coords);
                list.push_back(circle);
                break;
            }
            case 3: {
                float S;
                cout << "Enter S, circles less than it 'll be printed" << endl << "S = ";
                cin >> S;
                bool flag = false;
                for(int i = 0; i != list.size(); i++) 
                    if (list.at(i)->get_Square() < S) {
                        print_Data(list,i);
                        flag = true;
                    }
                if (!flag) cout << "There are no circles with square less then S" << endl;
                break;
            }
        }
    }
    cout << "Exit" << endl;
    for(vector<Circle*>::iterator it = list.begin(); it != list.end();it++) 
        delete (*it);
    return 0;
}

void print_Data(vector<Circle*> &list, int i) {
    cout << endl << "Circle №" << i+1 << endl;
    list.at(i)->Circle_Print();
}