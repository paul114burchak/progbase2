#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "country.h"
#include <QListWidgetItem>
#include <QUrl>
#include <QUrlQuery>
#include <QStringList>
#include <QNetworkReply>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QList <country> listOfCountries;
    void ListWidget_outputList(void);
//    void addCountries(QNetworkReply *reply);
//    void parseCountryList(QList <country> *listOfCountries);

private slots:
    void setBackground(void);
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_dateEdit_2_dateChanged(const QDate &date);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
