#include "country.h"

country::country(string key, string name, QDate startDate, int entryNumber, string citizen)
{
    this->_key = key;
    this->_entryNumber = entryNumber;
    this->_startDate = startDate;
    this->_name = name;
    this->_citizen = citizen;
}

int country::entryNumber() {
    return this->_entryNumber;
}

QDate country::startDate() {
    return this->_startDate;
}

string country::key() {
    return this->_key;
}

string country::name() {
    return this->_name;
}

string country::citizen() {
    return this->_citizen;
}
