
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "country.h"
#include <iostream>
#include <QDebug>
#include <list>
#include <QUrl>
#include <QUrlQuery>
#include <QList>
#include <iterator>
#include <QNetworkReply>
#include <QListWidgetItem>
#include <qjson-qt5/parser.h>
#include <progbase-cpp/net.h>
#include <QDateEdit>
#include <parser.h>

using namespace std;
using namespace QJson;
using namespace progbase::net;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QEventLoop eventLoop;
    QNetworkReply * reply;
    auto networkManager = new QNetworkAccessManager();
    try { connect(networkManager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
          QNetworkRequest req (QUrl(QString(QString::fromStdString("https://country.register.gov.uk/records.json"))));
          reply = networkManager->get(req);
          eventLoop.exec();
    } catch(NetException const & exc) {
        qDebug() << exc.what() << endl;
    }
    setBackground();
    ui->dateEdit->setEnabled(false);
    connection(&listOfCountries);
    ListWidget_outputList();
}

void MainWindow::setBackground(void){
    QPixmap bg (":/res/1.jpg");
    bg = bg.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bg);
    this->setPalette(palette);
}

//void MainWindow::parseCountryList(QList <country> *listOfCountries){
//        QEventLoop eventLoop;
//        auto networkManager = new QNetworkAccessManager();
//        try { connect(networkManager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
//              string url = "https://country.register.gov.uk/records.json";
//              QNetworkRequest req (QUrl(QString(QString::fromStdString(url))));
//              QNetworkReply *reply = networkManager->get(req);
//              eventLoop.exec();

//              if (reply->error() == QNetworkReply::NoError)
//              {
//                  addCountries(reply);
//                  qDebug() << "parseCountryList : Success" << endl;
//              }
//              else
//              {
//                  qDebug() << "parseCountryList : Failure" << endl;
//              }
//              delete reply;

//        } catch(NetException const & exc) {
//            qDebug() << exc.what() << endl;
//        }
//        qDebug() << "Countries added: " << listOfCountries->size() << endl;
//}

//void MainWindow::addCountries(QNetworkReply *reply){
//    QString json = reply->readAll();
//    QJson::Parser parser;
//    bool ok1;
//    QVariantMap key = parser.parse(json.toLatin1(), &ok1).toMap();
//    QString keys[] = {"PT","PW","PY","YE","QA","AE","ZA","ZM","ZW","RO","RS","RU","RW","SA","SB","SC","BS","SD","SE","SG","SI","SK","SL","SM","SN","SO","SR","SS","SZ","SY","KN","CH","UA","TG","TH","TJ","TM","CZ","LB","LC","TN","TO","TR","LI","LK","TT","TV","TZ"};
//    for (int i = 0; i < 48; i++) {
//        QString k = keys[i];
//        QVariantMap result = key[k].toMap();
//        QString num = result["entry-number"].toString();
//        QVariantList items = result["item"].toList();
//        if (!ok1) {
//          qDebug() << "Parsing error";
//          continue;
//        }
//        foreach(QVariant item, items)
//        {
//            QVariantMap Country = item.toMap();
//            QDate date = QDate::fromString(Country["start-date"].toString(),"yyyy-MM-dd");
//            country tmp = country(Country["country"].toString().toStdString(),
//                                      Country["name"].toString().toStdString(),
//                                      date,
//                                      num.toInt(),
//                                      Country["citizen-names"].toString().toStdString());
//            listOfCountries.push_back(tmp);
//        }
//    }
//}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    string name = (item->text()).toStdString();
    QList<country>::iterator it = listOfCountries.begin();
    for (int i = 0; i < (int) listOfCountries.size() && it != listOfCountries.end(); it++, i++)
    {
        string itemName = (*it).name();
        if (itemName.compare(name) == 0)
        {
            ui->label_5->setText(QString::fromStdString(name));
            ui->label_7->setText(QString::fromStdString((*it).key()));
            if ((*it).startDate().QDate::isNull()){
                qDebug() << "no date" << endl;
                ui->dateEdit->setDate(QDate::fromString("0001-01-01","yyyy-MM-dd"));
            }
            ui->dateEdit->setDate((*it).startDate());
            ui->label_11->setText(QString::number((*it).entryNumber()));
            ui->label_13->setText(QString::fromStdString((*it).citizen()));
        }
    }
}

void MainWindow::ListWidget_outputList(void){
    QListWidget * objectsZone = ui->listWidget;
    QList <country>::iterator it = listOfCountries.begin();
    int i;
    for (i = 0, it = listOfCountries.begin(); i < (int) listOfCountries.size() && it != listOfCountries.end(); it++, i++)
    {
        string name = (*it).name();
        QString qName = QString::fromStdString(name);
        QListWidgetItem *item = new QListWidgetItem;
        item->setText(qName);
        objectsZone->addItem(item);
    }
}

void MainWindow::on_dateEdit_2_dateChanged(const QDate &date)
{
    while(ui->listWidget->count()>0) {
          delete ui->listWidget->takeItem(0);
    }
    QListWidget * objectsZone = ui->listWidget;
    QList <country>::iterator it = listOfCountries.begin();
    int i;
    for (i = 0, it = listOfCountries.begin(); i < (int) listOfCountries.size() && it != listOfCountries.end(); it++, i++)
    {
        if (date.year() < (*it).startDate().year() || (date.year() == (*it).startDate().year() && date.month() < (*it).startDate().month()) || (date.year() == (*it).startDate().year() && date.month() == (*it).startDate().month() && date.day() < (*it).startDate().day())) {
            string name = (*it).name();
            QString qName = QString::fromStdString(name);
            QListWidgetItem *item = new QListWidgetItem;
            item->setText(qName);
            objectsZone->addItem(item);
        }
    }
}
