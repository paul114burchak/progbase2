#include <parser.h>

using namespace std;
using namespace QJson;
using namespace progbase::net;

void connection(QList <country> *listOfCountries){
        QEventLoop eventLoop;
        auto networkManager = new QNetworkAccessManager();
        try { QObject::connect(networkManager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
              string url = "https://country.register.gov.uk/records.json";
              QNetworkRequest req (QUrl(QString(QString::fromStdString(url))));
              QNetworkReply *reply = networkManager->get(req);
              eventLoop.exec();

              if (reply->error() == QNetworkReply::NoError)
              {
                  addCountries(reply,listOfCountries);
                  qDebug() << "parseCountryList : Success" << endl;
              }
              else
              {
                  qDebug() << "parseCountryList : Failure" << endl;
              }
              delete reply;

        } catch(NetException const & exc) {
            qDebug() << exc.what() << endl;
        }
        qDebug() << "Countries added: " << listOfCountries->size() << endl;
}

void addCountries(QNetworkReply *reply,QList <country> *listOfCountries){
    QString json = reply->readAll();
    QJson::Parser parser;
    bool ok1;
    QVariantMap key = parser.parse(json.toLatin1(), &ok1).toMap();
    QString keys[] = {"PT","PW","PY","YE","QA","AE","ZA","ZM","ZW","RO","RS","RU","RW","SA","SB","SC","BS","SD","SE","SG","SI","SK","SL","SM","SN","SO","SR","SS","SZ","SY","KN","CH","UA","TG","TH","TJ","TM","CZ","LB","LC","TN","TO","TR","LI","LK","TT","TV","TZ"};
    for (int i = 0; i < sizeof(keys)/sizeof(keys[0]); i++) {
        parseCountry(keys[i],key,listOfCountries,ok1);
    }
}

void parseCountry(QString keys, QVariantMap key,QList <country> *listOfCountries,bool ok1) {
    QString k = keys;
    QVariantMap result = key[k].toMap();
    QString num = result["entry-number"].toString();
    QVariantList items = result["item"].toList();
    if (!ok1) {
      qDebug() << "Parsing error";
      return;
    }
    foreach(QVariant item, items)
    {
        addCountry(item,listOfCountries,num);
    }
}

void addCountry(QVariant item,QList <country> *listOfCountries,QString num) {
    QVariantMap Country = item.toMap();
    QDate date = QDate::fromString(Country["start-date"].toString(),"yyyy-MM-dd");
    country tmp = country(Country["country"].toString().toStdString(),
                              Country["name"].toString().toStdString(),
                              date,
                              num.toInt(),
                              Country["citizen-names"].toString().toStdString());
    listOfCountries->push_back(tmp);
}
