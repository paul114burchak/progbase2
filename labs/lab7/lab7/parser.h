#ifndef PARSER_H
#define PARSER_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "country.h"
#include <iostream>
#include <QDebug>
#include <list>
#include <QUrl>
#include <QUrlQuery>
#include <QList>
#include <iterator>
#include <QNetworkReply>
#include <QListWidgetItem>
#include <qjson-qt5/parser.h>
#include <progbase-cpp/net.h>
#include <QDateEdit>

using namespace std;
using namespace QJson;
using namespace progbase::net;

    void addCountries(QNetworkReply *reply,QList <country> *listOfCountries);
    void connection(QList <country> *listOfCountries);
    void parseCountry(QString keys, QVariantMap key, QList<country> *listOfCountries,bool ok1);
    void addCountry(QVariant item, QList <country> *listOfCountries, QString num);

#endif // PARSER_H

