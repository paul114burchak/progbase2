#ifndef COUNTRY_H
#define COUNTRY_H

#include <iostream>
#include <QDate>

using namespace std;

class country
{
    string _key;
    string _name;
    QDate _startDate;
    int _entryNumber;
    string _citizen;
public:
    country(string key, string name, QDate startDate, int entryNumber, string citizen);
    string key();
    string name();
    QDate startDate();
    int entryNumber();
    string citizen();
};

#endif // COUNTRY_H
