#-------------------------------------------------
#
# Project created by QtCreator 2017-04-04T00:51:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab6
TEMPLATE = app

CONFIG +=c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    internetprovider.cpp \
    dialog.cpp \
    dialog1.cpp

HEADERS  += mainwindow.h \
    internetprovider.h \
    dialog.h \
    dialog1.h

FORMS    += mainwindow.ui \
    dialog.ui \
    dialog1.ui
