#ifndef INTERNETPROVIDER_H
#define INTERNETPROVIDER_H

#include <iostream>
using namespace std;

class IPBoss
{
    string fullname;
    int age;
    float salary;
public:
    IPBoss(string fullname, int age,float salary);
    string fullnameBoss();
    int ageBoss();
    float salaryBoss();
};

class InternetProvider
{
    string _name;
    int _speed;
    float _tariff;
    IPBoss * boss;
public:
    InternetProvider(string name ,int speed, float tariff, string fullname, int age,float salary);
    InternetProvider(InternetProvider * self);
    ~InternetProvider();
    string name();
    int speed();
    float tariff();
    string fullname();
    int age();
    float salary();
};


#endif // INTERNETPROVIDER_H
