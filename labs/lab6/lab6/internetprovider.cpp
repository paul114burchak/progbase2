#include "internetprovider.h"


IPBoss::IPBoss(string fullname, int age,float salary) {
    this->fullname = fullname;
    this->age = age;
    this->salary = salary;
}

string IPBoss::fullnameBoss() {
    return this->fullname;
}

int IPBoss::ageBoss() {
    return this->age;
}

float IPBoss::salaryBoss() {
    return this->salary;
}

InternetProvider::InternetProvider(string name ,int speed, float tariff, string fullname, int age,float salary)
{
    this->_name = name;
    this->_speed = speed;
    this->_tariff = tariff;
    this->boss = new IPBoss(fullname,age,salary);
}

InternetProvider::InternetProvider(InternetProvider * self) {
    this->_name = self->name();
    this->_speed = self->speed();
    this->_tariff = self->tariff();
    this->boss = new IPBoss(self->fullname(),self->age(),self->salary());
}

InternetProvider::~InternetProvider(){}

string InternetProvider::name() {
    return this->_name;
}

int InternetProvider::speed() {
    return this->_speed;
}

float InternetProvider::tariff() {
    return this->_tariff;
}

string InternetProvider::fullname() {
    return boss->fullnameBoss();
}

int InternetProvider::age() {
    return boss->ageBoss();
}

float InternetProvider::salary() {
    return boss->salaryBoss();
}
