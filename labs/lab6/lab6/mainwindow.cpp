#include "mainwindow.h"
#include <vector>
#include <algorithm>
#include "internetprovider.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "ui_dialog.h"
#include <QCloseEvent>
#include "dialog1.h"
#include "ui_dialog1.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    Dialog *dialog = new Dialog();
    dialog->show();
    if (dialog->exec()) {
        QLineEdit * nameEdit = dialog->findChild<QLineEdit*>("lineEdit_1");
        QSpinBox * speed = dialog->findChild<QSpinBox*>("spinBox");
        QDoubleSpinBox * tariff = dialog->findChild<QDoubleSpinBox*>("doubleSpinBox");
        QLineEdit * fullnameEdit = dialog->findChild<QLineEdit*>("lineEdit_4");
        QSpinBox * age = dialog->findChild<QSpinBox*>("spinBox_2");
        QDoubleSpinBox * salary = dialog->findChild<QDoubleSpinBox*>("doubleSpinBox_2");
        InternetProvider * provider = new InternetProvider(nameEdit->text().toStdString(),speed->value(), tariff->value(),fullnameEdit->text().toStdString(),age->value(), salary->value());
        this->providers.push_back(provider); // @TODO add to listwidget

        QListWidgetItem *qproviderListItem = new QListWidgetItem();
        qproviderListItem->setText(QString::fromStdString(provider->name()));
        ui->listWidget->addItem(qproviderListItem);
    }
    dialog->hide();
    //delete dialog;
}

void MainWindow::on_pushButton_2_clicked()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

        foreach(QListWidgetItem * item, items)
        {
            QString deleted = item->text();
            int i = 0;
            for(; i < ui->listWidget->count(); ++i)
            {
                QListWidgetItem* tmp = ui->listWidget->item(i);
                QString temp = tmp->text();
                if (QString::compare(deleted, temp, Qt::CaseInsensitive) == 0) {
                    break;
                }
            }
            this->providers.erase(providers.begin()+i);
            delete ui->listWidget->takeItem(ui->listWidget->row(item));
            ui->label_8->clear();
            ui->label_10->clear();
            ui->label_11->clear();
            ui->label_13->clear();
            ui->label_14->clear();
            ui->label_15->clear();
        }
}

void MainWindow::on_listWidget_itemSelectionChanged()
{
    auto items = ui->listWidget->selectedItems();
    bool hasSelected = items.size() > 0;
    if (hasSelected) {
        auto text = items[0]->text();
        int i = 0;
        for(; i < ui->listWidget->count(); ++i)
        {
            QListWidgetItem* tmp = ui->listWidget->item(i);
            QString temp = tmp->text();
            if (QString::compare(text, temp, Qt::CaseInsensitive) == 0) {
                break;
            }
        }
        if ( i >= providers.size()) i--;
        QString name = QString::fromStdString(this->providers.at(i)->name());
        ui->label_8->setText(name);
        QString speed = QString::number(this->providers.at(i)->speed());
        ui->label_10->setText(speed);
        QString tariff = QString::number(this->providers.at(i)->tariff());
        ui->label_11->setText(tariff);
        QString fullname = QString::fromStdString(this->providers.at(i)->fullname());
        ui->label_14->setText(fullname);
        QString age = QString::number(this->providers.at(i)->age());
        ui->label_15->setText(age);
        QString salary = QString::number(this->providers.at(i)->salary());
        ui->label_13->setText(salary);
    }
    ui->pushButton_2->setEnabled(hasSelected);
}

void MainWindow::on_pushButton_3_clicked()
{
    while(ui->listWidget_2->count()>0) {
      delete ui->listWidget_2->takeItem(0);
    }
    ui->label_16->clear();
    ui->label_17->clear();
    ui->label_18->clear();
    Dialog1 * dialog = new Dialog1();
    dialog->show();
    unsigned int num;
    if (dialog->exec()) {
        QSpinBox * spin = dialog->findChild<QSpinBox*>("spinBox");
        num = spin->value();
    }
    dialog->hide();
    delete dialog;
    vector <InternetProvider *> sorted(providers);
    struct {
            bool operator()(InternetProvider * a, InternetProvider * b)
            {
                return a->tariff() > b->tariff();
            }
    } comparator;
    std::sort(sorted.begin(),sorted.end(), comparator);
    if (num <= sorted.size()) {
        for(unsigned int i = 0; i < num;i++) {
            QListWidgetItem *qproviderListItem = new QListWidgetItem();
            qproviderListItem->setText(QString::fromStdString(sorted.at(i)->name()));
            ui->listWidget_2->addItem(qproviderListItem);
        }
    } else {
        ui->label_16->setText(QString::fromStdString("Spinbox num is greater than number of objects"));
        ui->label_17->setText(QString::fromStdString("Please enter another number less than"));
        ui->label_18->setText(QString::number(this->providers.size()+1));
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    for (InternetProvider * st : providers) {
    delete st;
}
event->accept();
}
