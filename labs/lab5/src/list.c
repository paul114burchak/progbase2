#include <list.h>
#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <glob.h>

List_internet_prov * List_internet_prov_new(void) {
	List_internet_prov * list = (List_internet_prov *)malloc(sizeof(List_internet_prov));
	list->head = NULL;
	list->next = NULL;
	return list;
}

void List_internet_prov_free(List_internet_prov ** self) {
	assert(NULL != self);
	free(*self);
	*self = NULL;
}

List_internet_prov * internet_prov_new() {
	List_internet_prov * node = (List_internet_prov *)malloc(sizeof(List_internet_prov));
	node->next = NULL;
	node->head = NULL;
	return node;
}
void List_internet_prov_set(List_internet_prov * self,Provider prov) {
	self->data = prov;
}

Provider* List_internet_prov_get(List_internet_prov * self) {
	Provider * x = &self->data;
	return x;
}
//?
List_internet_prov * internet_prov_AddNode(List_internet_prov * self, List_internet_prov * List) {
	struct List_internet_prov * cur = NULL;// (struct List_internet_prov*) malloc(sizeof(List_internet_prov));
	if (self->head == NULL) {
		self->head = List;
		self->data = List->data;
		self->next = NULL;
		return self;
	}
	cur = self->head;
	while (cur->next != NULL) {
		cur = cur->next;
	}
	List->head = self->head;
	cur->next = List;
	return self;
}

void internet_prov_removeFirst(List_internet_prov * self) {
	List_internet_prov * node = self->head;
	if (node == NULL) assert(0);
	self->head = node->next;
	List_internet_prov_free(&node);
}

void internet_prov_removeAt(List_internet_prov * self, int position) {
	assert(position >= 0);
	if (position == 0) {
		internet_prov_removeFirst(self);
	}
	int i = 0;
	List_internet_prov * x = self->head;
	while (x->next->next != NULL && i != position - 1) {
    	  i += 1;
    	  x = x->next;
	}
	//if (i != position - 1) assert(0);
	List_internet_prov * node = x->next;
	x->next = node->next;
	List_internet_prov_free(&node);
}

int internet_prov_count(List_internet_prov * self) {
	int count = 0;
	List_internet_prov * x = self->head;
	while (x != NULL) {
		count++;
		x = x->next;
	}
	return count;
}

void ListPrint(List_internet_prov * self) {
	int k = -1;
	List_internet_prov * x = self->head;
	for (int j = 0; j < internet_prov_count(self); j++) {
		k++;
		if (k > 2) {
			k = 0;
		}
		int p = j / 3;
		conSetAttr(FG_RED);
		conMove(1+(7*p),(k*50)+1);
		printf("Cтруктура №%i:",j+1);
		conReset();
		conMove(2+(7*p),(k*50)+1);
		printf("1) Назва:%s", x->data.name);
		conMove(3+(7*p),(k*50)+1);
		printf("2) Швидкість:%i Mбіт/сек", x->data.speed);
		conMove(4+(7*p),(k*50)+1);
		printf("3) Тариф:%.3f грн/міс", x->data.tariff);
		conMove(5+(7*p),(k*50)+1);
		printf("4.1) Ім'я босса:%s", x->data.boss.fullname);
		conMove(6+(7*p),(k*50)+1);
		printf("4.2) Вік:%i", x->data.boss.age);
		conMove(7+(7*p),(k*50)+1);
		printf("4.3) Стать:%s", x->data.boss.sex);
		x = x->next;
	}
}

void freeData(List_internet_prov * List) {
	List_internet_prov* x = List->head;
	List_internet_prov* x_next;
	for (int i = 0; i < internet_prov_count(List);i++) {
		x_next = x->next;
		List_internet_prov_free(&x);
		x=x_next;
	}
	List_internet_prov_free(&List);
}

List_internet_prov* internet_provNodeByIndex(List_internet_prov * List, int i) {
	List_internet_prov * y = List->head;
	for (int k = 0; y != NULL && k != i;k++) y = y->next; 
	return y;
}
