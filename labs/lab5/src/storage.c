#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <struct.h>
#include <glob.h>
#include <list.h>
#include <storage.h>
#include <jansson.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

List_internet_prov * Storage_readAsTxt(const char * readFileName) {
	FILE * fin = fopen(readFileName, "r+");
	if ( fin == NULL) {
		return NULL;
	}
	List_internet_prov * list = List_internet_prov_new();
	while (!feof(fin)) {
		List_internet_prov * provider = internet_prov_new();
		int speed;
		float tariff;
		int age;
		fscanf(fin,"%s\n%i\n%f\n%s\n%i\n%s\n\n", provider->data.name, &speed, &tariff, provider->data.boss.fullname,&age, provider->data.boss.sex);
		provider->data.speed = speed;
		provider->data.tariff = tariff;
		provider->data.boss.age = age;
		internet_prov_AddNode(list,provider);
	}
	fclose(fin);
	return list;
}

int Storage_writeAsTxt(List_internet_prov * provider, const char * writeFileName) {
	FILE * fout = fopen(writeFileName, "w+");
	if (fout == NULL) {
		return EXIT_FAILURE;
	}
	while (provider != NULL) {
		int speed = provider->data.speed;
		float tariff = provider->data.tariff;
		int age = provider->data.boss.age;
		fprintf(fout,"%s\n%u\n%f\n%s\n%u\n%s\n\n", provider->data.name, speed, tariff, provider->data.boss.fullname, age, provider->data.boss.sex);
		provider = provider->next;
	}
	fclose(fout);
	return 0;
}

List_internet_prov * Storage_defineReadFileType (const char * readFileName) {
	char *format = malloc(sizeof(char)*5);
	//char format[4];
	memset(format,0,5);
	for (int i = strlen(readFileName)-1,j = 0;j < 5;i--, j++) {
		if (readFileName[i] == '.') break;
		format[j] = readFileName[i];
	}
	List_internet_prov * self;
	if (strcmp(format,"nosj")==0) self = Storage_readAsJson(readFileName);
	else if (strcmp(format,"lmx")==0) self = Storage_readAsXml(readFileName);
	else self = Storage_readAsTxt(readFileName);

	free(format);
	return self;
}

int Storage_defineWriteFileType (List_internet_prov * self, const char * writeFileName) {
	char *format = malloc(sizeof(char)*4);
	memset(format,0,4);
	for (int i = strlen(writeFileName)-1,j = 0;j < 4;i--, j++) {
		if (writeFileName[i] == '.') break;
		format[j] = writeFileName[i];
	}
	int f = 0;
	if (strcmp(format,"nosj")==0) f = Storage_writeAsJson(writeFileName, self);
	else if (strcmp(format,"lmx")==0) f = Storage_writeAsXml(writeFileName, self);
	else f = Storage_writeAsTxt(self,writeFileName);
	free(format);
	return f;
}


int Storage_writeAsJson(const char * writeFileName, List_internet_prov * provider) {
	FILE * fout = fopen(writeFileName, "w+");
	if (fout == NULL) {
		return EXIT_FAILURE;
	}
	char str[10000];
	json_t* arr = json_array();
	for(int i = 0; i < internet_prov_count(provider); i++) {
		json_t * prov = json_object();
        json_object_set_new(prov, "name" , json_string(provider->data.name));
        json_object_set_new(prov, "speed" , json_integer(provider->data.speed));
        json_object_set_new(prov, "tariff" , json_real(provider->data.tariff));

		json_t * boss = json_object();
        json_object_set_new(boss, "fullname" , json_string(provider->data.boss.fullname));
        json_object_set_new(boss, "age" , json_integer(provider->data.boss.age));
        json_object_set_new(boss, "sex" , json_string(provider->data.boss.sex));
		json_object_set_new(prov, "boss", boss);
		json_array_insert_new(arr,i,prov);
		provider = provider->next;
	}

	char * jsonString = json_dumps(arr, JSON_INDENT(2));
    strcpy(str, jsonString);
    free(jsonString); 
    json_decref(arr);

	fprintf(fout,"%s", str);
	fclose(fout);
	return 0;
}


List_internet_prov * Storage_readAsJson(const char * readFileName) {
	FILE * fin = fopen(readFileName, "r+");
	if ( fin == NULL) {
		return NULL;
	}

	char * buf = ReadAllText(fin);

	fclose(fin);

	json_error_t err;
	
	json_t * jsonArr = json_loads(buf, 0, &err);
	int index = 0;
	json_t * value = NULL;
	List_internet_prov * self = List_internet_prov_new();
	json_array_foreach(jsonArr, index, value) {
		List_internet_prov * provider = internet_prov_new();
		json_t * groupObj = json_object_get(value, "boss");
		char* name;
		name = (char *)json_string_value(json_object_get(value, "name"));
		strcpy(provider->data.name,name);
		provider->data.speed = json_integer_value(json_object_get(value, "speed"));
		provider->data.tariff = json_real_value(json_object_get(value, "tariff"));
		char* bossName;
		bossName = (char *)json_string_value(json_object_get(groupObj, "fullname"));
		strcpy(provider->data.boss.fullname,bossName);
		provider->data.boss.age = json_integer_value(json_object_get(groupObj, "age"));
		char* bossSex;
		bossSex = (char *)json_string_value(json_object_get(groupObj, "sex"));
		strcpy(provider->data.boss.sex,bossSex);
		internet_prov_AddNode(self,provider);
	};
	free(buf);
	json_decref(jsonArr);
	return self;
}


int Storage_writeAsXml(const char * writeFileName, List_internet_prov * provider) {
	FILE * fout = fopen(writeFileName, "w+");
	if (fout == NULL) {
		return EXIT_FAILURE;
	}

	xmlDoc * doc = NULL;
	xmlNode * rootNode = NULL;
	xmlNode * providerNode = NULL;
	//xmlNode * providerNode = NULL;
	char strBuf[1000];

	doc = xmlNewDoc(BAD_CAST "1.0");

	rootNode = xmlNewNode(NULL, BAD_CAST "providers");
	xmlDocSetRootElement(doc, rootNode);

	while (provider != NULL) {
		providerNode = xmlNewChild(rootNode, NULL, BAD_CAST "provider", NULL);
		xmlNewChild(providerNode, NULL, BAD_CAST "name", BAD_CAST provider->data.name);
		sprintf(strBuf, "%i", provider->data.speed);  // copy number to string
		xmlNewChild(providerNode, NULL, BAD_CAST "speed", BAD_CAST strBuf);
		sprintf(strBuf, "%f", provider->data.tariff);  // copy number to string
		xmlNewChild(providerNode, NULL, BAD_CAST "tariff", BAD_CAST strBuf);

		//providerNode = xmlNewChild(providerNode, NULL, BAD_CAST "boss", NULL);
		xmlNewChild(providerNode, NULL, BAD_CAST "fullname", BAD_CAST provider->data.boss.fullname);
		sprintf(strBuf, "%i", provider->data.boss.age);  // copy number to string
		xmlNewChild(providerNode, NULL, BAD_CAST "age", BAD_CAST strBuf);
		xmlNewChild(providerNode, NULL, BAD_CAST "sex", BAD_CAST provider->data.boss.sex);
		provider = provider->next;
	}
		
	xmlBuffer * bufferPtr = xmlBufferCreate();
	xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
	for (int i = 0; bufferPtr->content[i] != '\0'; i++) {
		fprintf(fout, "%c", bufferPtr->content[i]);
	}
	xmlBufferFree(bufferPtr);

	fclose(fout);
	xmlFreeDoc(doc);
	return 0;
}


List_internet_prov * Storage_readAsXml(const char * readFileName) {
	FILE * fin = fopen(readFileName, "r+");
	if (fin == NULL) {
		return NULL;
	}
	char *buf = ReadAllText(fin);
	
	fclose(fin);
	xmlDoc * xDoc = xmlReadMemory(buf, strlen(buf), NULL, NULL, 0);
    if (NULL == xDoc) {
        printf("Error parsing xml");
        return NULL;
    }

	List_internet_prov* list = List_internet_prov_new();
	xmlNode * xRootEl = xmlDocGetRootElement(xDoc);
	for(xmlNode * xprovider = xRootEl->children; NULL != xprovider ; xprovider = xprovider->next) { 
		if (XML_ELEMENT_NODE == xprovider->type) {
			List_internet_prov* provider = internet_prov_new();
			for (xmlNode * xJ = xprovider->children; NULL != xJ ; xJ = xJ->next) {
				if (XML_ELEMENT_NODE == xJ->type) {
					char * content = (char *)xmlNodeGetContent(xJ);
					//puts(content);
					if(xmlStrcmp(xJ->name, BAD_CAST "name") == 0)
					strcpy(provider->data.name,(char*) content);
					 if(xmlStrcmp(xJ->name, BAD_CAST "speed") == 0)
						provider->data.speed = atoi((char *)content);
					 if(xmlStrcmp(xJ->name, BAD_CAST "tariff") == 0) 
						provider->data.tariff = atof((char *)content);

					if(xmlStrcmp(xJ->name, BAD_CAST "fullname") == 0) 
						strcpy(provider->data.boss.fullname ,(char*) content);
					if(xmlStrcmp(xJ->name, BAD_CAST "age") == 0)
						provider->data.boss.age = atoi((char *)content);
					if(xmlStrcmp(xJ->name, BAD_CAST "sex") == 0) 
						strcpy(provider->data.boss.sex ,(char*) content);
					xmlFree(content);
				}
			}
			internet_prov_AddNode(list, provider);
		}
	}
	free(buf);
	xmlFreeDoc(xDoc);
	return list;
}

char * ReadAllText(FILE * fp) {
	int n = 10, delta = 10,i = 0;   
	char* buf = (char*) malloc (sizeof(char)*n);    
	while ((buf [i] = fgetc(fp)) != EOF  )  {                
		if (++i >= n) {
			n += delta;
			buf = (char*) realloc (buf, sizeof(char)*n);        
		}
	}
	buf[i] = '\0';
	return buf;
}
