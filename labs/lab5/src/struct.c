#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <struct.h>
#include <prints.h>
#include <glob.h>
#include <list.h>

enum { BUFFER_SIZE = 100 };


void createNew(char input[][30]) {
	char print[6][60];
	strcpy(print[0],"1) Назва: ");
	strcpy(print[1],"2) Швидкість: ");
	strcpy(print[2],"3) Тариф: ");
	strcpy(print[3],"4.1) Ім'я босса: ");
	strcpy(print[4],"4.2) Вік: ");
	strcpy(print[5],"4.3) Стать: ");

	for(int i = 0; i < 6;i++) {
		print[i][strlen(print[i]) - 1] = '\0';
		printf("%s",print[i]);
		fgets(input[i],BUFFER_SIZE,stdin);
		input[i][strlen(input[i]) - 1] = '\0';
	}
}

void StrToStruct(struct Provider * provider, char str[][30]) {
	strcpy(provider->name,str[0]);
	provider->speed = atoi(str[1]);
	provider->tariff = atof(str[2]);
	strcpy(provider->boss.fullname,str[3]);
	provider->boss.age = atoi(str[4]);
	strcpy(provider->boss.sex,str[5]);
}

void StructToStr(struct Provider *provider, char str[][30]) {
	sprintf(str[0],"%s ",provider->name);
	sprintf(str[1],"%u ",provider->speed);
	sprintf(str[2],"%.3f ",provider->tariff);
	sprintf(str[3],"%s ", provider->boss.fullname);
	sprintf(str[4],"%u ",provider->boss.age);
	sprintf(str[5],"%s ",provider->boss.sex);
}

void RewriteStruct(struct Provider * provider1, struct Provider * provider2) {
	char str[6][30];
	StructToStr(provider2,str);
	StrToStruct(provider1,str);
}

void RewriteStructField(struct Provider * provider, int field, char * str) {
	switch (field) {
		case 0:
			strcpy(provider->name,str);
		break;
		case 1:
			provider->speed = atoi(str);
		break;
		case 2:
			provider->tariff = atof(str);
		break;
		case 3:
			strcpy(provider->boss.fullname,str);
		break;
		case 4:
			provider->boss.age = atoi(str);
		break;
		case 5:
			strcpy(provider->boss.sex,str);
		break;
	}
}

int compare(float x, float y) {
	if (x > y) return -1;
	else if (y > x) return 1;
	else return 0;
}

List_internet_prov* insertToSortedList(List_internet_prov* head, List_internet_prov* n) {
		if (head == NULL) {
			return n;
		}
		
		if (compare(n->data.tariff,head->data.tariff) == -1) {
			n->next = head;
			head = n;
			return head;
		}
		
		List_internet_prov* search = head;
		while (search->next != NULL && compare(n->data.tariff,search->next->data.tariff) == 1) {
			search = search->next;
		}
		n->next =search->next;
	    search->next = n;
		return head;
}

List_internet_prov* Top_K_Structs(List_internet_prov* head,int k) {
	List_internet_prov* sortedList = head;
	head = head->next;
	sortedList->next = NULL;
	int i = 0;
	while (head != NULL && i!=k) {
		i++;
		List_internet_prov* insertedEl = head;
		head = head->next;
		insertedEl->next=NULL;
		sortedList = insertToSortedList(sortedList, insertedEl);
	}
	return sortedList;
}

