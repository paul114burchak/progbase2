#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <struct.h>

void menuPrint(void) {
	conClear();
	conMove(1,1);
	printf("\t\t\t\t\t\tЛабораторна робота №3 (Інтернет-провайдер)\n");
	conMove(3,30);
	conSetAttr(FG_RED);
	printf("Створити новий масив даних");
	conReset();
	conMove(3,93);
	printf("Зчитати масив даних із файлу");
	conHideCursor();
}

void exitlab(void) {
	printf("\nНатисніть Backspace, щоб вийти в головне меню\n");
	int chs = 0;
	while (chs != 127) {
		chs = conGetChar();
	}
}

void printTaskMenu(void) {
	conMove(1,1);
	printf("\t\t\t\t\t\tЛабораторна робота №3 (Інтернет-провайдер)\n");
	conMove(5,5);
	printf("Structs to textfile");
	conMove(6,5);
	printf("Textfile to Structs");
	conMove(7,5);
	printf("Delete struct");
	conMove(8,5);
	printf("Copy struct");
	conMove(9,5);
	printf("Rewrite struct field");
	conMove(10,5);
	printf("К провайдерів з найдорожчим тарифом");
	conMove(11,5);
	printf("Existed structs to textfile");
	conMove(12,5);
	printf("Existed structs on screen");
	conMove(13,5);
	printf("Rewrite struct by index");
	conMove(14,5);
	printf("Add struct to List");
}
