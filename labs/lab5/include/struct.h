#ifndef STRUCT_H
#define STRUCT_H

#include "list.h"

void createNew(char input[][30]);
void taskMenu(List_internet_prov *List, int lim);
void StrToStruct(struct Provider * provider, char str[][30]);
void StructToStr(struct Provider *provider, char str[][30]);
void RewriteStruct(struct Provider * provider1, struct Provider * provider2);
void RewriteStructField(struct Provider * provider, int field, char * str);
List_internet_prov* Top_K_Structs(List_internet_prov* head,int k);
List_internet_prov* insertToSortedList(List_internet_prov* head, List_internet_prov* n);
int compare(float x, float y);

#endif