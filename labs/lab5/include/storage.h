#ifndef STORAGE_H
#define STORAGE_H

#include "list.h"
#include <stdio.h>

int Storage_writeAsTxt(List_internet_prov * self, const char * writeFileName);
List_internet_prov * Storage_readAsTxt(const char * readFileName);
int Storage_writeAsJson(const char * writeFileName, List_internet_prov * self);
List_internet_prov * Storage_readAsJson(const char * readFileName);
int Storage_writeAsXml(const char * writeFileName, List_internet_prov * list);
List_internet_prov * Storage_readAsXml(const char * readFileName);
int Storage_defineWriteFileType (List_internet_prov * self, const char * writeFileName);
List_internet_prov * Storage_defineReadFileType (const char * readFileName);
char * ReadAllText(FILE * fin);

#endif