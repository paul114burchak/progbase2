#ifndef GLOB_H
#define GLOB_H

struct Boss {
		char fullname[30];
		int age;
		char sex[30];
};
struct Provider {
	char name[30];
	int speed;
	float tariff;
	struct Boss boss;
};

struct List_internet_prov {
	struct List_internet_prov * head;
    struct List_internet_prov * next;
    struct Provider data;
};

#endif