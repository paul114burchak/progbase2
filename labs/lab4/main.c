#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "menu.h"
#include "struct.h"
#include "prints.h"
#include "list.h"
#include "file.h"

int main(int argc, char* argv[]) {
	//if (argc > 1 && !strcmp(argv[1],"t")) {
	//	tests();
	//}
	int ch = 0;
	int index = 0;
	List_internet_prov *List;
	List = List_internet_prov_new();
	menuPrint();
	while (ch != 127) {
		ch = conGetChar();
		conReset();
		conMove(3,30);
		printf("Створити новий масив даних");
		conMove(3,93);
		printf("Зчитати масив даних із файлу");
		
		index = menu(ch,1,index,68,65);
		
		if (index == 0) {
			conMove(3,30);
			conSetAttr(FG_RED);
			printf("Створити новий масив даних");
			conReset();
		}
		if (index == 1) {
			conMove(3,93);
			conSetAttr(FG_RED);
			printf("Зчитати масив даних із файлу");
			conReset();
		}
		if (ch == 10) {
			conClear();
			switch (index) {
				case 0: {
					int i = 1;
					i = countStruct(i,10,1);
					conClear();
					conShowCursor();
					for (int j = 0; j < i;j++) {
						char str[6][30];
						conMove(1,1);
						printf("Введіть структури, заповняючи поля в такому порядку: \n1) Назва:\n2) Швидкість:\n3) Тариф:\n4.1) Ім'я босса:\n4.2) Вік:\n4.3) Стать:");
						conMove(2,1);
						createNew(str);
						//TODO
						internet_prov *provider = internet_prov_new();
						List = internet_prov_AddNode(List,provider); 
						StrToStruct(provider, str);
						conClear();
					}
					ListPrint(List);
					exitlab();
					taskMenu(List,i);
				} break;
				case 1:{
					int i = 10;
					int f = 0;
					char filename[20];
					conShowCursor();
					printf("Введіть назву файлу з якого будуть зчитані дані (structs.txt): ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					conHideCursor();
					conClear();
					//TODO
					for (int j = 0; j < i;j++) {
						internet_prov *provider = internet_prov_new();
						List = internet_prov_AddNode(List,provider);
					}
					f = TextToStructs(List,i,filename);
					if (f) {
						freeData(List);
						List = List_internet_prov_new();
						i = 1;
						internet_prov *provider = internet_prov_new();
						List = internet_prov_AddNode(List,provider);
						f = TextToStructs(List,i,"empty.txt");
						conClear();
						printf("Файл пустий або не існує");
					} else {
						ListPrint(List);
					}
					exitlab();
					taskMenu(List,i);
				} break;
			}
		}
	}
	freeData(List);
	conClear();
	conShowCursor();
	puts("Best wishes!");
	return 0;
}
