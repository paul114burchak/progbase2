#ifndef STRUCT_H
#define STRUCT_H

#include "list.h"

void createNew(char input[][30]);
void taskMenu(List_internet_prov *List, int lim);
void StrToStruct(struct internet_prov * provider, char str[][30]);
void StructToStr(struct internet_prov *provider, char str[][30]);
void RewriteStruct(struct internet_prov * provider1, struct internet_prov * provider2);
void RewriteStructField(struct internet_prov * provider, int field, char * str);
internet_prov* Top_K_Structs(internet_prov* head,int k);
internet_prov* insertToSortedList(internet_prov* head, internet_prov* n);
int compare(float x, float y);

#endif