#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "struct.h"
#include "prints.h"
#include "menu.h"
#include "list.h"
#include "file.h"

struct List_internet_prov {
	struct internet_prov * head;
};

void taskMenu(List_internet_prov *List, int lim) {
	int cht = 0;
	int indext = 0;
	conClear();
	conHideCursor();
	conMove(4,5);
	conSetAttr(FG_RED);
	printf("Struct to String");
	conReset();
	printTaskMenu();
	while (cht != 127) {
		cht = conGetChar();
		conReset();
		conMove(4,5);
		printf("Struct to String");
		printTaskMenu();
		indext = menu(cht,10,indext,83,87);
		ifsMenu(indext);
		if (cht == 10) {
			switch (indext) {
				case 0: {
					int i = 1;
					char str[6][30];
					i = countStruct(i,lim,1);
					internet_prov * cur = internet_provNodeByIndex(List,i-1);
					StructToStr(cur, str);
					conMove(6,3);
					printf("%s %s %s %s %s %s ", str[0], str[1], str[2], str[3], str[4], str[5]);
					backToMenu();
				} break;
				case 1: {
					char filename[20];
					int f = 0;
					conClear();
					printf("Введіть назву файлу в який будуть записані дані: ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					f = StructsToText(List, filename);
					if (f == 1) {
						printf("Smth wrong");
					}
					conHideCursor();
					backToMenu();
				} break;
				case 2: {
					int i = 1;
					int f = 0;
					freeData(List);
					List = List_internet_prov_new();
					i = countStruct(i,10,1);
					conMove(1,1);
					printf("                                     ");
					char filename[20];
					printf("Введіть назву файлу з якого будуть зчитані дані (structs.txt): ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					for (int j = 0; j < i;j++) {
						internet_prov *provider = internet_prov_new();
						List = internet_prov_AddNode(List,provider);
					}
					f = TextToStructs(List,i,filename);
					conClear();
					if (f) {
						freeData(List);
						List = List_internet_prov_new();
						i = 1;
						internet_prov *provider = internet_prov_new();
						List = internet_prov_AddNode(List,provider);
						f = TextToStructs(List,i,"empty.txt");
						conClear();
						printf("Файл пустий або не існує");
					} else {
						if(i > lim) {
							lim = i;
						}
						ListPrint(List); 
					}
					backToMenu();
				} break;
				case 3: {
					if (lim > 1){
						int i = 1;
						i = countStruct(i,lim,1);
						internet_prov_removeAt(List,i-1);
						conMove(5,1);
						printf("Дані зі Структури №%i очищені", i);
						backToMenu();
						lim--;
					} else {
						printf("Список - путсий");
						freeData(List);
						exit(0);
					}
				} break;
				case 4: {
					int i = 1;
					int j = 1;
					int ch = 0;
					i = countStruct(i,lim,1);
					conMove(4,1);
					printf("Введіть індекс структури, що копіюється:  %i  ", j);
					while ( ch != 10) {
						ch = conGetChar();
						j = FuncMenu(ch,j,lim);
						conMove(4,1);
						printf("Введіть індекс структури, що копіюється:  %i  ", j);
					}
					
					internet_prov * x = internet_provNodeByIndex(List,i-1);
					internet_prov * y = internet_provNodeByIndex(List,j-1);
					RewriteStruct(x,y);
					backToMenu();
				} break;
				case 5: {
					int i = 1;
					int field = 0;
					int ch = 0;
					char str[30];
					i = countStruct(i,lim,1);
					conMove(4,1);
					printf("0-Назва;1-Швидкість;2-Тариф(float);3-Ім'я босса;4-Вік;5-Стать\n");
					printf("Номер поля:  %i  ",field);
					while ( ch != 10) {
						ch = conGetChar();
						field = menu(ch, 5, field, 87, 83);
						conMove(5,1);
						printf("Номер поля:  %i  ",field);
					}
					conMove(6,1);
					printf("Нове значення поля: ");
					fgets(str,30,stdin);
					str[strlen(str) - 1] = '\0';
					internet_prov * x = internet_provNodeByIndex(List,i-1);
					RewriteStructField(x, field, str);
					backToMenu();
				} break;
				case 6:{
					int k = 1;
					int ch = 0;
					conClear();
					conMove(1,1);
					printf("K = %i  \n", k);
					conSetAttr(FG_RED);
					printf("Використовуйте W та S для вибору");
					conReset();
					while ( ch != 10) {
						ch = conGetChar();
						k = FuncMenu(ch,k,lim);
						conMove(1,1);
						printf("K = %i  ", k);
					}
					List->head = Top_K_Structs(List->head, 9);
					conClear();
					for (int j = k;j < lim;j++) internet_prov_removeAt(List,j);
					if (lim!=0) ListPrint(List);
					lim = k;
					backToMenu();
				} break;
				case 7: {
					char filename[20];
					int f = 0;
					conClear();
					printf("Введіть назву файлу в який будуть записані дані: ");
					fgets(filename,20,stdin);
					filename[strlen(filename) - 1] = '\0';
					StructsToText(List, filename);
					if (f == 1) {
						printf("Smth wrong");
					}
					backToMenu();
				} break;
				case 8: {
					conClear();
					ListPrint(List);
					backToMenu();
				} break;
				case 9: {
					int i = 1;
					char str[6][30];
					i = countStruct(i,lim,1);
					conClear();
					conShowCursor();
					conMove(1,1);
					printf("Введіть структурy, заповняючи поля в такому порядку: \n1) Назва:\n2) Швидкість:\n3) Тариф:\n4.1) Ім'я босса:\n4.2) Вік:\n4.3) Стать:");
					conMove(2,1);
					createNew(str);
					internet_prov * x = internet_provNodeByIndex(List,i-1);
					StrToStruct(x, str);
					conClear();
					conHideCursor();
					backToMenu();
				} break;
				case 10: {
					char str[6][30];
					conClear();
					conShowCursor();
					conMove(1,1);
					printf("Введіть структурy, заповняючи поля в такому порядку: \n1) Назва:\n2) Швидкість:\n3) Тариф:\n4.1) Ім'я босса:\n4.2) Вік:\n4.3) Стать:");
					conMove(2,1);
					createNew(str);
					internet_prov* node = internet_prov_new();
					List = internet_prov_AddNode(List,node);
					StrToStruct(node, str);
					conClear();
					conHideCursor();
					backToMenu();
					lim++;
				}
			}
			indext = 0;
		}
	}
	menuPrint();
}

int FuncMenu (int ch, int j, int lim) {
	if (ch == 119 || ch == 87) {
		j++;
	}
	if (ch == 115 || ch == 83) {
		j--;
	}
	if (j < 1) {
		j = lim;
	}
	if (j > lim) {
		j = 1;
	}
	return j;
}

int menu (int ch, int i, int index,int butP, int butM) {
	if (ch == butP || ch == (butP+32)) {
		index++;
	}
	if (ch == butM || ch == (butM+32)) {
		index--;
	}
	if (index < 0) {
		index = i;
	}
	if (index > i) {
		index = 0;
	}
	return index;
}

int countStruct (int index, int h, int l) {
	int ch = 0;
	conClear();
	conMove(1,1);
	printf("\t\t\t\t\t\tЛабораторна робота №3 (Інтернет-провайдер)\n");
	printf("Введіть кількість(номер) структур(и):  %i  ", index);
	conMove(3,10);
	conSetAttr(FG_RED);
	printf("Використовуйте W та S для вибору");
	conReset();
	while ( ch != 10) {
		ch = conGetChar();
		if (ch == 119 || ch == 87) {
			index++;
		}
		if (ch == 115 || ch == 83) {
			index--;
		}
		if (index < l) {
			index = h;
		}
		if (index > h) {
			index = l;
		}
		conMove(2,1);
		printf("Введіть кількість(номер) структур(и):  %i  ", index);
	}
	return index;
}

void ifsMenu(int indext) {
	char str[11][100];
	strcpy(str[0],"Struct to String");
	strcpy(str[1],"Structs to textfile");
	strcpy(str[2],"Textfile to structs");
	strcpy(str[3],"Delete struct");
	strcpy(str[4],"Copy struct");
	strcpy(str[5],"Rewrite struct field");
	strcpy(str[6],"К провайдерів з найдорожчим тарифом");
	strcpy(str[7],"Existed structs to textfile");
	strcpy(str[8],"Existed structs on screen");
	strcpy(str[9],"Rewrite struct by index");
	strcpy(str[10],"Add struct to List");
	for (int i = 0; i < 11; i++) {
		if (indext == i) {
			conMove(indext+4,5);
			conSetAttr(FG_RED);
			printf("%s",str[i]);
			conReset();
			break;
		}
	}
}

void backToMenu(void) {
	exitlab();
	conClear();
	conMove(4,5);
	conSetAttr(FG_RED);
	printf("Struct to String");
	conReset();
	printTaskMenu();
}
