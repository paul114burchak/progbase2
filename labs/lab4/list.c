#include "list.h"
#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

struct Boss {
		char fullname[30];
		int age;
		char sex[30];
};
struct internet_prov {
	char name[30];
	int speed;
	float tariff;
	struct Boss boss;
	internet_prov * next;
};

struct List_internet_prov {
	struct internet_prov * head;
};

List_internet_prov * List_internet_prov_new(void) {
	List_internet_prov * list = (List_internet_prov *)malloc(sizeof(List_internet_prov));
	list->head = NULL;
	return list;
}

void List_internet_prov_free(List_internet_prov ** self) {
	assert(NULL != self);
	free(*self);
	*self = NULL;
}

internet_prov * internet_prov_new() {
	internet_prov * node = (internet_prov *)malloc(sizeof(internet_prov));
	node->next = NULL;
	return node;
}

void internet_prov_free(internet_prov ** self) {
	assert(NULL != self);
	free(*self);
	*self = NULL;
}
//?
List_internet_prov* internet_prov_AddNode(List_internet_prov * self, internet_prov * node) {
	if (self->head == NULL) {
		node->next = self->head;
		self->head = node;
		return self;
	}
	internet_prov * cur = self->head;
	while (cur->next != NULL) {
		cur = cur->next;
	}
	cur->next = node;
	return self;
}

void internet_prov_removeFirst(List_internet_prov * self) {
	internet_prov * node = self->head;
	if (node == NULL) assert(0);
	self->head = node->next;
	internet_prov_free(&node);
}

void internet_prov_removeAt(List_internet_prov * self, int position) {
	assert(position >= 0);
	if (position == 0) {
		internet_prov_removeFirst(self);
	}
	int i = 0;
	internet_prov * cur = self->head;
	while (cur->next->next != NULL && i != position - 1) {
    	  i += 1;
    	  cur = cur->next;
	}
	//if (i != position - 1) assert(0);
	internet_prov * node = cur->next;
	cur->next = node->next;
	internet_prov_free(&node);
}

int internet_prov_count(List_internet_prov * self) {
	int count = 0;
	internet_prov * cur = self->head;
	while (cur != NULL) {
		count++;
		cur = cur->next;
	}
	return count;
}

void ListPrint(List_internet_prov * self) {
	int k = -1;
	internet_prov * cur = self->head;
	for (int j = 0; j < internet_prov_count(self); j++) {
		k++;
		if (k > 2) {
			k = 0;
		}
		int p = j / 3;
		conSetAttr(FG_RED);
		conMove(1+(7*p),(k*50)+1);
		printf("Cтруктура №%i:",j+1);
		conReset();
		conMove(2+(7*p),(k*50)+1);
		printf("1) Назва:%s", cur->name);
		conMove(3+(7*p),(k*50)+1);
		printf("2) Швидкість:%i Mбіт/сек", cur->speed);
		conMove(4+(7*p),(k*50)+1);
		printf("3) Тариф:%.3f грн/міс", cur->tariff);
		conMove(5+(7*p),(k*50)+1);
		printf("4.1) Ім'я босса:%s", cur->boss.fullname);
		conMove(6+(7*p),(k*50)+1);
		printf("4.2) Вік:%i", cur->boss.age);
		conMove(7+(7*p),(k*50)+1);
		printf("4.3) Стать:%s", cur->boss.sex);
		cur = cur->next;
	}
}

void freeData(List_internet_prov * List) {
	internet_prov* cur = List->head;
	internet_prov* cur_next;
	for (int i = 0; i < internet_prov_count(List);i++) {
		cur_next = cur->next;
		internet_prov_free(&cur);
		cur=cur_next;
	}
	List_internet_prov_free(&List);
}

internet_prov* internet_provNodeByIndex(List_internet_prov * List, int i) {
	internet_prov * y = List->head;
	for (int k = 0; y != NULL && k != i;k++) y = y->next; 
	return y;
}
