#ifndef FILE_H
#define FILE_H

#include "list.h"

int TextToStructs(List_internet_prov * self, int i, const char * readFileName);
int StructsToText(List_internet_prov * self, const char * writeFileName);

#endif