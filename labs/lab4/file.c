#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "struct.h"
#include "prints.h"
#include "list.h"
#include "file.h"

struct Boss {
		char fullname[30];
		int age;
		char sex[30];
};
struct internet_prov {
	char name[30];
	int speed;
	float tariff;
	struct Boss boss;
	internet_prov * next;
};

struct List_internet_prov {
	struct internet_prov * head;
};

int TextToStructs(List_internet_prov * self, int i, const char * readFileName) {
	FILE * fin = fopen(readFileName, "r+");
	if ( fin == NULL) {
		return EXIT_FAILURE;
	}
	internet_prov * cur = self->head;
	for (int j = 0; j < i && cur != NULL; j++) { 
		int speed;
		float tariff;
		int age;
		fscanf(fin,"%s\n%i\n%f\n%s\n%i\n%s\n\n", cur->name, &speed, &tariff, cur->boss.fullname,&age, cur->boss.sex);
		cur->speed = speed;
		cur->tariff = tariff;
		cur->boss.age = age;
		cur = cur->next;
	}
	fclose(fin);
	return 0;
}

int StructsToText(List_internet_prov * self, const char * writeFileName) {
	FILE * fout = fopen(writeFileName, "w+");
	if (fout == NULL) {
		return EXIT_FAILURE;
	}
	internet_prov * cur = self->head;
	while (cur != NULL) {
		int speed = cur->speed;
		float tariff = cur->tariff;
		int age = cur->boss.age;
		fprintf(fout,"%s\n%u\n%f\n%s\n%u\n%s\n\n", cur->name, speed, tariff, cur->boss.fullname, age, cur->boss.sex);
		cur = cur->next;
	}
	fclose(fout);
	return 0;
}