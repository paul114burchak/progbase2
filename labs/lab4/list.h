#ifndef LIST_H
#define LIST_H

typedef struct Boss Boss;
typedef struct internet_prov internet_prov;
typedef struct List_internet_prov List_internet_prov;
List_internet_prov * List_internet_prov_new(void);
void List_internet_prov_free(List_internet_prov ** self);
internet_prov * internet_prov_new();
void internet_prov_free(internet_prov ** self);
List_internet_prov* internet_prov_AddNode(List_internet_prov * self, internet_prov * node);
void internet_prov_removeFirst(List_internet_prov * self);
void internet_prov_removeAt(List_internet_prov * self, int position);
int internet_prov_count(List_internet_prov * self);
void ListPrint(List_internet_prov * self);
void freeData(List_internet_prov * List);
internet_prov* internet_provNodeByIndex(List_internet_prov * List, int i);

#endif