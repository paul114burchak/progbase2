#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include "struct.h"

struct Boss {
		char fullname[30];
		int age;
		char sex[30];
};
struct internet_prov {
	char name[30];
	int speed;
	float tariff;
	struct Boss boss;
	internet_prov * next;
};

struct List_internet_prov {
	struct internet_prov * head;
};



START_TEST (toStr_string_equality)
{
    
	struct internet_prov test1 = {"C", 1000, 300.000, {"Ritchie",70, "male"} };
	char str1 [6][30];
    StructToStr(&test1, str1);
    char testStr1 [6][30] = {{"C "}, {"1000 "}, {"300.000 "} ,{"Ritchie "} ,{"70 "} ,{"male "}};
    for (int i = 0 ; i < 6; i++)
	ck_assert_str_eq(str1[i], testStr1[i]);
	

	struct internet_prov test2 = {"", 2016, 300.000, {"Ritchie",70, "male"} };
	char str2 [6][30];
    StructToStr(&test2, str2);
    char testStr2 [6][30] = {{" "}, {"2016 "}, {"300.000 "} ,{"Ritchie "} ,{"70 "} ,{"male "}};
    for (int i = 0 ; i < 6; i++)
	ck_assert_str_eq(str2[i], testStr2[i]);


	struct internet_prov test3 = {"", 1, 301.000, {"",3, ""} };
	char str3 [6][30];
    StructToStr(&test3, str3);
    char testStr3 [6][30] = {{" "}, {"1 "}, {"301.000 "} ,{" "} ,{"3 "} ,{" "}};
    for (int i = 0 ; i < 6; i++)
	ck_assert_str_eq(str3[i], testStr3[i]);
}
END_TEST


START_TEST (field_struct_equality){

	struct internet_prov test;
	char str1[30] = "kpipzks";
	RewriteStructField(&test, 0, str1);
	ck_assert_str_eq(test.name, str1);

	char str2[30] = "2357";
	RewriteStructField(&test, 1, str2);
	ck_assert_int_eq(test.speed, 2357);

	char str3[30] = "";
	RewriteStructField(&test, 1, str3);
	ck_assert_int_eq(test.speed, 0);

	char str4[30] = "ewfbwkm";
	RewriteStructField(&test, 2, str4);
	ck_assert_float_eq(test.tariff, 0);

	char str5[30] = "4.3";
	RewriteStructField(&test, 5, str5);
	ck_assert_str_eq(test.boss.sex, str5);
}
END_TEST


START_TEST (toStruct_struct_equality){

	struct internet_prov test[1];
	char str[6][30] = { {"name\0"},{"99\0"},{""},{"boss\0"},{"19\0"},{"female\0"}};
	StrToStruct(test,str);

	ck_assert_str_eq(str[0],test[0].name);
	ck_assert_str_eq(str[5],test[0].boss.sex);
	ck_assert_int_eq(test[0].speed , 99);
	ck_assert_int_eq(test[0].boss.age ,19);
}
END_TEST


START_TEST (element_struct_equality){

	struct internet_prov test[2] = {
	{ "KOTIK",554, 32.7,{ "erv",55, "creature"}},
	{"0",0,0.0,{"jhg",2,"male"}}
	};
	RewriteStruct(&test[0],&test[1]);
	ck_assert_str_eq(test[0].name, test[0].name);
	ck_assert_int_eq(test[1].speed , 0);
	ck_assert_int_eq(test[1].boss.age , 2);
	ck_assert_int_eq((test[0].boss.age), 2);
}
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Module");
    TCase * tc_structure = tcase_create("TestCase");

    tcase_add_test(tc_structure,toStr_string_equality);
    tcase_add_test(tc_structure,field_struct_equality);
    tcase_add_test(tc_structure,toStruct_struct_equality);
    tcase_add_test(tc_structure,element_struct_equality);
    suite_add_tcase(s,tc_structure);
    return s;
}

int main(void) {

  Suite *s = test_suite();
  SRunner *sr = srunner_create(s);
  srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!

  srunner_run_all(sr, CK_VERBOSE);

  int num_tests_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return num_tests_failed;
}

